<?php

define('WATCHDOG_DEBUG', 7);

function smartling_log_get_handler() {
  return NULL;
}

function watchdog($type, $message, $variables = array(), $severity = WATCHDOG_NOTICE, $link = NULL) {}

function drupal_register_shutdown_function() {}

function format_string($string, array $args = array()) {}

/**
 * SmartlingLogTest.
 */
class SmartlingLogTest extends \PHPUnit_Framework_TestCase {

  /**
   * Returns mocked logger.
   *
   * @param bool $log_mode
   * @param bool $cloud_log_mode
   * @param int $flush_called
   * @return PHPUnit_Framework_MockObject_MockObject
   */
  private function getLogger($log_mode = TRUE, $cloud_log_mode = TRUE, $flush_called = 1) {
    $settings = $this->getMockBuilder('\Drupal\smartling\Settings\SmartlingSettingsHandler')
      ->setMethods([])
      ->getMock();

    $settings->expects($this->any())
      ->method('getLogMode')
      ->willReturn($log_mode);

    $settings->expects($this->any())
      ->method('getCloudLogMode')
      ->willReturn($cloud_log_mode);

    $logger = $this->getMockBuilder('\Drupal\smartling\Log\SmartlingLog')
      ->setMethods(['flush'])
      ->setConstructorArgs([$settings])
      ->getMock();

    $logger->expects($this->exactly($flush_called))
      ->method('flush');

    return $logger;
  }

  /**
   * Test buffer logic: log 100 messages with buffer = 100 items. Logging
   * is enabled. Cloud logging is enabled.
   * Expected result: flush() called once.
   */
  public function testBufferSize100Records100FlushOnce() {
    $logger = $this->getLogger(TRUE, TRUE, 1);

    foreach (range(1, 100) as $item) {
      $logger->debug('Test ' . $item);
    }
  }

  /**
   * Test buffer logic: log 200 messages with buffer = 100 items. Logging
   * is enabled. Cloud logging is enabled.
   * Expected result: flush() called twice.
   */
  public function testBufferSize100Records200FlushTwice() {
    $logger = $this->getLogger(TRUE, TRUE, 2);

    foreach (range(1, 200) as $item) {
      $logger->debug('Test ' . $item);
    }
  }

  /**
   * Test cloud logging: log 100 messages with buffer = 100 items. Logging is
   * enabled. Cloud logging is enabled.
   * Expected result: flush() called once.
   */
  public function testLoggingEnabledCloudLoggingEnabled() {
    $logger = $this->getLogger(TRUE, TRUE, 1);

    foreach (range(1, 100) as $item) {
      $logger->debug('Test ' . $item);
    }
  }

  /**
   * Test cloud logging: log 100 messages with buffer = 100 items. Logging is
   * enabled. Cloud logging is disabled.
   * Expected result: flush() is never called.
   */
  public function testLoggingEnabledCloudLoggingDisabled() {
    $logger = $this->getLogger(TRUE, FALSE, 0);

    foreach (range(1, 100) as $item) {
      $logger->debug('Test ' . $item);
    }
  }

  /**
   * Test cloud logging: log 100 messages with buffer = 100 items. Logging is
   * disabled. Cloud logging is enabled.
   * Expected result: flush() is never called.
   */
  public function testLoggingDisabledCloudLoggingEnabled() {
    $logger = $this->getLogger(FALSE, TRUE, 0);

    foreach (range(1, 100) as $item) {
      $logger->debug('Test ' . $item);
    }
  }

  /**
   * Test cloud logging: log 100 messages with buffer = 100 items. Logging is
   * disabled. Cloud logging is disabled.
   * Expected result: flush() is never called.
   */
  public function testLoggingDisabledCloudLoggingDisabled() {
    $logger = $this->getLogger(FALSE, FALSE, 0);

    foreach (range(1, 100) as $item) {
      $logger->debug('Test ' . $item);
    }
  }

}
