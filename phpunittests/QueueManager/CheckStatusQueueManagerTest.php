<?php

use Drupal\smartling\QueueManager;

/**
 * @file
 * Tests for smartling.
 */
function t($str) {
}

function url($url) {
}

/**
 * SmartlingFileCleanTest.
 */
class CheckStatusQueueManagerTest extends \PHPUnit_Framework_TestCase {

  private $submission;

  public function setUp() {
    $this->api_wrapper = $this->getMockBuilder('\Drupal\smartling\ApiWrapper\SmartlingApiWrapper')
      ->disableOriginalConstructor()
      ->getMock();

    $this->smartling_submission_wrapper = $this->getMockBuilder('\Drupal\smartling\Wrappers\SmartlingEntityDataWrapper')
      ->disableOriginalConstructor()
      ->getMock();

    $this->submissions_collection = $this->getMockBuilder('\Drupal\smartling\Wrappers\SmartlingEntityDataWrapperCollection')
      ->disableOriginalConstructor()
      ->getMock();

    $this->queue_download = $this->getMockBuilder('\Drupal\smartling\QueueManager\DownloadQueueManager')
      ->disableOriginalConstructor()
      ->getMock();

    $this->log = $this->getMockBuilder('\Drupal\smartling\Log\SmartlingLog')
      ->disableOriginalConstructor()
      ->getMock();

    $this->smartling_utils = $this->getMockBuilder('\Drupal\smartling\Wrappers\SmartlingUtils')
      ->disableOriginalConstructor()
      ->getMock();

    $this->drupal_wrapper = $this->getMockBuilder('\Drupal\smartling\Wrappers\DrupalAPIWrapper')
      ->disableOriginalConstructor()
      ->getMock();

    $this->submission = new stdClass();
    $this->submission->file_name = 'file_name';
    $this->submission->target_language = 'nl';
    $this->submission->last_modified = 0;
    $this->submission->eid = 1;
    $this->submission->progress = 0;
    $this->submission->entityType = 'smartling_entity_data';
  }


  public function testExecuteNotConfigured() {
    $this->setExpectedException('\Drupal\smartling\SmartlingExceptions\SmartlingNotConfigured');

    $this->smartling_utils->expects($this->once())
      ->method('isConfigured')
      ->will($this->returnValue(FALSE));

    $this->queue_download->expects($this->never())
      ->method('add');

    $manager = new QueueManager\CheckStatusQueueManager($this->api_wrapper, $this->smartling_submission_wrapper, $this->submissions_collection, $this->queue_download, $this->log, $this->smartling_utils, $this->drupal_wrapper);
    $manager->execute(1);
  }

  public function testExecuteEmptyStatusCheckResult() {
    $this->smartling_utils->expects($this->once())
      ->method('isConfigured')
      ->will($this->returnValue(TRUE));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('loadByID')
      ->will($this->returnValue($this->smartling_submission_wrapper));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('getEntity')
      ->will($this->returnValue($this->submission));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('setEntity')
      ->will($this->returnValue($this->smartling_submission_wrapper));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('save');

    $this->api_wrapper->expects($this->once())
      ->method('getLastModified')
      ->will($this->returnValue(array('nl' => 12346)));

    $this->api_wrapper->expects($this->once())
      ->method('getStatusAllLocales')
      ->will($this->returnValue(array('nl' => array('authorizedStringCount' => 1, 'completedStringCount' => 0))));

    $response = [
      'authorizedStringCount' => 1,
      'completedStringCount' => 0,
    ];

    $this->api_wrapper->expects($this->once())
      ->method('getStatus')
      ->will($this->returnValue(array(
        'response_data' => $response,
        'entity_data' => NULL,
      )));

    $this->queue_download->expects($this->never())
      ->method('add');

    $manager = new QueueManager\CheckStatusQueueManager($this->api_wrapper, $this->smartling_submission_wrapper, $this->submissions_collection, $this->queue_download, $this->log, $this->smartling_utils, $this->drupal_wrapper);
    $manager->execute(1);

    $this->assertEquals(TRUE, TRUE);
  }

  public function testExecuteDoNotDownloadYet1() {
    $this->smartling_utils->expects($this->once())
      ->method('isConfigured')
      ->will($this->returnValue(TRUE));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('loadByID')
      ->will($this->returnValue($this->smartling_submission_wrapper));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('setEntity')
      ->will($this->returnValue($this->smartling_submission_wrapper));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('save');


    $this->smartling_submission_wrapper->expects($this->once())
      ->method('getEntity')
      ->will($this->returnValue($this->submission));

    $response = [
      'authorizedStringCount' => 1,
      'completedStringCount' => 1,
    ];

    $this->api_wrapper->expects($this->once())
      ->method('getStatus')
      ->will($this->returnValue(array(
        'response_data' => $response,
        'entity_data' => NULL,
      )));

    $this->api_wrapper->expects($this->once())
      ->method('getLastModified')
      ->will($this->returnValue(array('nl' => 12346)));

    $this->api_wrapper->expects($this->once())
      ->method('getStatusAllLocales')
      ->will($this->returnValue(array('nl' => array('authorizedStringCount' => 1, 'completedStringCount' => 1))));

    $this->queue_download->expects($this->never())
      ->method('add');

    $manager = new QueueManager\CheckStatusQueueManager($this->api_wrapper, $this->smartling_submission_wrapper, $this->submissions_collection, $this->queue_download, $this->log, $this->smartling_utils, $this->drupal_wrapper);
    $manager->execute(1);

    $this->assertEquals(TRUE, TRUE);
  }

  public function testExecuteDoDownload() {
    $this->smartling_utils->expects($this->once())
      ->method('isConfigured')
      ->will($this->returnValue(TRUE));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('loadByID')
      ->will($this->returnValue($this->smartling_submission_wrapper));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('setEntity')
      ->will($this->returnValue($this->smartling_submission_wrapper));

    $this->smartling_submission_wrapper->expects($this->once())
      ->method('save');


    $smartling_submission = $this->submission;
    $smartling_submission->entity_type = 'node';
    $this->smartling_submission_wrapper->expects($this->once())
      ->method('getEntity')
      ->will($this->returnValue($smartling_submission));

    $response = [
      'authorizedStringCount' => 0,
      'completedStringCount' => 2,
    ];

    $this->api_wrapper->expects($this->once())
      ->method('getStatus')
      ->will($this->returnValue(array(
        'response_data' => $response,
        'entity_data' => NULL,
      )));

    $this->queue_download->expects($this->once())
      ->method('add');

    $this->api_wrapper->expects($this->once())
      ->method('getLastModified')
      ->will($this->returnValue(array('nl' => 12346)));

    $this->api_wrapper->expects($this->once())
      ->method('getStatusAllLocales')
      ->will($this->returnValue(array('nl' => array('authorizedStringCount' => 0, 'completedStringCount' => 2))));

    $manager = new QueueManager\CheckStatusQueueManager($this->api_wrapper, $this->smartling_submission_wrapper, $this->submissions_collection, $this->queue_download, $this->log, $this->smartling_utils, $this->drupal_wrapper);
    $manager->execute(1);

    $this->assertEquals(TRUE, TRUE);
  }
}
