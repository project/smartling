<?php

use Drupal\smartling\Settings\SmartlingSettingsHandler;
use Drupal\smartling\SmartlingApiFactory;
use Smartling\BaseApiAbstract;
use Smartling\Batch\BatchApi;
use Smartling\Context\ContextApi;
use Smartling\File\FileApi;
use Smartling\Jobs\JobsApi;
use Smartling\Project\ProjectApi;

define('SMARTLING_PRODUCTION_MODE', 'PRODUCTION');

/**
 * Mocked system_get_info().
 *
 * Needed for SmartlingApiFactory::getModuleVersion() method.
 *
 * @see system_get_info().
 */
function system_get_info($type, $name = NULL) {
  return [];
}

/**
 * @file
 * Tests for smartling.
 */

class ApiFactoryTest extends \PHPUnit_Framework_TestCase {

  private $settingsMock;

  /**
   * Test mixture.
   */
  public function setUp() {
    $this->settingsMock = $this->getMock(SmartlingSettingsHandler::class);
  }

  /**
   * Test factory: request unsupported API.
   *
   * @expectedException Exception
   * @expectedExceptionMessage Unsupported API has been requested: unsupportedAPI
   */
  public function testCreateUnsupportedApi() {
    SmartlingApiFactory::create($this->settingsMock, 'unsupportedAPI');
  }

  /**
   * Test factory: request supported APIs.
   *
   * @param array $data
   *
   * @dataProvider testCreateAPIObjectsDataProvider
   */
  public function testCreateAPIObjects(array $data) {
    $apiObject = SmartlingApiFactory::create($this->settingsMock, $data['type']);
    $this->assertEquals('drupal-classic-connector', BaseApiAbstract::getCurrentClientId());
    $this->assertEquals('7.x-4.x-dev', BaseApiAbstract::getCurrentClientVersion());
    $this->assertInstanceOf($data['class'], $apiObject);
  }

  public function testCreateAPIObjectsDataProvider() {
    return [
      [
        ['type' => 'file', 'class' => FileApi::class],
      ],
      [
        ['type' => 'project', 'class' => ProjectApi::class],
      ],
      [
        ['type' => 'jobs', 'class' => JobsApi::class],
      ],
      [
        ['type' => 'batch', 'class' => BatchApi::class],
      ],
      [
        ['type' => 'context', 'class' => ContextApi::class],
      ],
    ];
  }

}
