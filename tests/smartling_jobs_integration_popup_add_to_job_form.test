<?php

/**
 * @file
 * Tests for smartling.
 */

/**
 * SmartlingJobsIntegrationPopupAddToJobFormTest.
 */
class SmartlingJobsIntegrationPopupAddToJobFormTest extends SmartlingWebTestCase {

  /**
   * Test info.
   *
   * @return array
   *   Return test info.
   */
  public static function getInfo() {
    return array(
      'name' => 'Smartling Jobs/Batch API integration popup add to job form',
      'description' => 'Test popup form submission',
      'group' => 'Smartling',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $node = new stdClass();
    $node->title = 'test';
    $node->type = 'article';
    $node->language = 'en';
    $node->uid = 1;
    $node->status = 1;
    $node->promote = 0;
    $node->comment = 1;

    node_save($node);

    $this->smartlingSetCorrectSettings();
    $this->smartlingSetRetrievalType('pseudo');
    $this->smartlingSetMethodSettings('article');
    $this->smartlingSetFieldSettings('article', 'title_property_field');

    module_enable(['smartling_translate_page_popup']);
  }

  /**
   * Validate due date.
   */
  public function testPopupAddToJobDueDateValidation() {
    $this->drupalGet('/node/1');
    $edit = [
      'items[1_||_node]' => '1_||_node',
      'add_to_existing_job_tab[container][job_info][description]' => 'Description ' . $this->randomString(),
      'add_to_existing_job_tab[container][job_info][due_date][date]' => '12/12/2015',
      'add_to_existing_job_tab[container][job_info][due_date][time]' => '12:12',
      'add_to_existing_job_tab[container][job_info][authorize]' => TRUE,
      'add_to_existing_job_tab[container][job_info][target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $commands = $this->drupalPostAJAX(NULL, $edit, [
      'op' => t('Add to job'),
    ]);

    $this->assertIdentical($commands[2]['data'], '<div id="translation_result" class="failed">Due date can not be in the past.</div>');
  }

  /**
   * Validate target locales.
   */
  public function testPopupAddToJobTargetLocaleValidation() {
    $this->drupalGet('/node/1');
    $edit = [
      'items[1_||_node]' => '1_||_node',
      'add_to_existing_job_tab[container][job_info][description]' => 'Description ' . $this->randomString(),
      'add_to_existing_job_tab[container][job_info][due_date][date]' => '12/12/2020',
      'add_to_existing_job_tab[container][job_info][due_date][time]' => '12:12',
      'add_to_existing_job_tab[container][job_info][authorize]' => TRUE,
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $commands = $this->drupalPostAJAX(NULL, $edit, [
      'op' => t('Add to job'),
    ]);

    $this->assertIdentical($commands[2]['data'], '<div id="translation_result" class="failed">Job Target Locales field is required.</div>');
  }

  /**
   * Validate items.
   */
  public function testPopupAddToJobItemsValidation() {
    $this->drupalGet('/node/1');
    $edit = [
      'add_to_existing_job_tab[container][job_info][description]' => 'Description ' . $this->randomString(),
      'add_to_existing_job_tab[container][job_info][due_date][date]' => '12/12/2020',
      'add_to_existing_job_tab[container][job_info][due_date][time]' => '12:12',
      'add_to_existing_job_tab[container][job_info][authorize]' => TRUE,
      'add_to_existing_job_tab[container][job_info][target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $commands = $this->drupalPostAJAX(NULL, $edit, [
      'op' => t('Add to job'),
    ]);

    $this->assertIdentical($commands[2]['data'], '<div id="translation_result" class="failed">Content entities field is required.</div>');
  }

  /**
   * Add to job sync.
   */
  public function testPopupAddToJobSync() {
    $conf_edit = [
      "async_mode" => FALSE,
    ];
    $this->drupalPost('admin/config/regional/smartling', $conf_edit, t('Save'), [], [], 'smartling-admin-expert-settings-form');

    $this->drupalGet('/node/1');
    $edit = [
      'items[1_||_node]' => '1_||_node',
      'add_to_existing_job_tab[container][job_info][description]' => 'Description ' . $this->randomString(),
      'add_to_existing_job_tab[container][job_info][due_date][date]' => '12/12/2020',
      'add_to_existing_job_tab[container][job_info][due_date][time]' => '12:12',
      'add_to_existing_job_tab[container][job_info][authorize]' => TRUE,
      'add_to_existing_job_tab[container][job_info][target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPostAJAX(NULL, $edit, [
      'op' => t('Add to job'),
    ]);
    $this->drupalGet('/node/1');
    $this->assertText(t('The node "test" has been sent to Smartling for translation to "nl".'));

    $submission = entity_load_single('smartling_entity_data', 1);
    $smartling_submission = drupal_container()->get('smartling.wrappers.smartling_submission_wrapper')
      ->setEntity($submission);

    $this->drupalGet('/admin/reports/dblog');
    $this->assertText(t('Smartling updated a job'));
    $this->assertNoText(t('Smartling created a job'));
    $this->assertText(t('Smartling created a batch'));
    $this->assertText(t('Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertNoText(t('Fallback: Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertText(t('Smartling executed a batch'));
  }

  /**
   * Add to job async.
   */
  public function testPopupAddToJobAsync() {
    $this->drupalGet('/node/1');
    $edit = [
      'items[1_||_node]' => '1_||_node',
      'add_to_existing_job_tab[container][job_info][description]' => 'Description ' . $this->randomString(),
      'add_to_existing_job_tab[container][job_info][due_date][date]' => '12/12/2020',
      'add_to_existing_job_tab[container][job_info][due_date][time]' => '12:12',
      'add_to_existing_job_tab[container][job_info][authorize]' => TRUE,
      'add_to_existing_job_tab[container][job_info][target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPostAJAX(NULL, $edit, [
      'op' => t('Add to job'),
    ]);
    $this->drupalGet('/node/1');
    $this->assertText(t('The node "test" has been queued for translation in Smartling to "nl". File will be uploaded to Smartling by next cron runs.'));

    $submission = entity_load_single('smartling_entity_data', 1);
    $smartling_submission = drupal_container()->get('smartling.wrappers.smartling_submission_wrapper')
      ->setEntity($submission);

    $this->drupalGet('/admin/reports/dblog');
    $this->assertText(t('Smartling updated a job'));
    $this->assertNoText(t('Smartling created a job'));
    $this->assertText(t('Smartling created a batch'));
    $this->assertNoText(t('Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertNoText(t('Smartling executed a batch'));

    $this->processQueues();

    $this->drupalGet('/admin/reports/dblog');
    $this->assertText(t('Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertNoText(t('Fallback: Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertText(t('Smartling executed a batch'));
  }

}
