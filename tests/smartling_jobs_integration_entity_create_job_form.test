<?php

/**
 * @file
 * Tests for smartling.
 */

/**
 * SmartlingJobsIntegrationEntityCreateJobFormTest.
 */
class SmartlingJobsIntegrationEntityCreateJobFormTest extends SmartlingWebTestCase {

  /**
   * Test info.
   *
   * @return array
   *   Return test info.
   */
  public static function getInfo() {
    return array(
      'name' => 'Smartling Jobs/Batch API integration entity create job form',
      'description' => 'Test entity edit form submission',
      'group' => 'Smartling',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $node = new stdClass();
    $node->title = 'test';
    $node->type = 'article';
    $node->language = 'en';
    $node->uid = 1;
    $node->status = 1;
    $node->promote = 0;
    $node->comment = 1;

    node_save($node);

    $this->smartlingSetCorrectSettings();
    $this->smartlingSetRetrievalType('pseudo');
    $this->smartlingSetMethodSettings('article');
    $this->smartlingSetFieldSettings('article', 'title_property_field');
  }

  /**
   * Validate due date format.
   */
  public function testBulkCreateJobDueDateFormatValidation() {
    $this->drupalGet('admin/content/smartling-content');
    $edit = [
      "views_bulk_operations[0]" => 1,
    ];
    $this->drupalPost('admin/content/smartling-content', $edit, t('Create job'));

    $edit = [
      'create_new_job_tab[name]' => 'Test',
      'create_new_job_tab[description]' => 'Description ' . $this->randomString(),
      'create_new_job_tab[due_date][date]' => '2015-12-12',
      'create_new_job_tab[due_date][time]' => '12:12',
      'create_new_job_tab[authorize]' => TRUE,
      'create_new_job_tab[target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPost(NULL, $edit, t('Next'));
    $this->assertText(t('The value 2015-12-12 12:12 does not match the expected format.'));
  }

  /**
   * Validate empty job name.
   */
  public function testNodeCreateJobEmptyNameValidation() {
    $edit = [
      'create_new_job_tab[name]' => '',
      'create_new_job_tab[description]' => 'Description ' . $this->randomString(),
      'create_new_job_tab[due_date][date]' => '12/12/2020',
      'create_new_job_tab[due_date][time]' => '12:12',
      'create_new_job_tab[authorize]' => TRUE,
      'create_new_job_tab[target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPost('node/1/edit', $edit, t('Create job'));
    $this->assertText(t('Job Name field is required.'));
  }

  /**
   * Validate existing job name.
   */
  public function testNodeCreateJobExistingNameValidation() {
    $existing_job_name = 'Drupal classic connector test: EXISTING JOB';

    $edit = [
      'create_new_job_tab[name]' => $existing_job_name,
      'create_new_job_tab[description]' => 'Description ' . $this->randomString(),
      'create_new_job_tab[due_date][date]' => '12/12/2020',
      'create_new_job_tab[due_date][time]' => '12:12',
      'create_new_job_tab[authorize]' => TRUE,
      'create_new_job_tab[target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPost('node/1/edit', $edit, t('Create job'));

    $edit = [
      'create_new_job_tab[name]' => $existing_job_name,
      'create_new_job_tab[description]' => 'Description ' . $this->randomString(),
      'create_new_job_tab[due_date][date]' => '12/12/2020',
      'create_new_job_tab[due_date][time]' => '12:12',
      'create_new_job_tab[authorize]' => TRUE,
      'create_new_job_tab[target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPost('node/1/edit', $edit, t('Create job'));
    $this->assertText(t('Job with name "@name" already exists. Please choose another job name.', [
      '@name' => $existing_job_name,
    ]));
  }

  /**
   * Validate due date.
   */
  public function testNodeCreateJobDueDateValidation() {
    $edit = [
      'create_new_job_tab[name]' => 'Test',
      'create_new_job_tab[description]' => 'Description ' . $this->randomString(),
      'create_new_job_tab[due_date][date]' => '12/12/2015',
      'create_new_job_tab[due_date][time]' => '12:12',
      'create_new_job_tab[authorize]' => TRUE,
      'create_new_job_tab[target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPost('node/1/edit', $edit, t('Create job'));
    $this->assertText(t('Due date can not be in the past.'));
  }

  /**
   * Validate target locales.
   */
  public function testNodeCreateJobTargetLocaleValidation() {
    $edit = [
      'create_new_job_tab[name]' => 'Test',
      'create_new_job_tab[description]' => 'Description ' . $this->randomString(),
      'create_new_job_tab[due_date][date]' => '12/12/2020',
      'create_new_job_tab[due_date][time]' => '12:12',
      'create_new_job_tab[authorize]' => TRUE,
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPost('node/1/edit', $edit, t('Create job'));
    $this->assertText(t('Job Target Locales field is required.'));
  }

  /**
   * Create job sync.
   */
  public function testNodeCreateJobSync() {
    $conf_edit = [
      "async_mode" => FALSE,
    ];
    $this->drupalPost('admin/config/regional/smartling', $conf_edit, t('Save'), [], [], 'smartling-admin-expert-settings-form');

    $edit = [
      'create_new_job_tab[name]' => 'Drupal classic connector test ' . $this->randomName(),
      'create_new_job_tab[description]' => 'Description ' . $this->randomString(),
      'create_new_job_tab[due_date][date]' => '12/12/2020',
      'create_new_job_tab[due_date][time]' => '12:12',
      'create_new_job_tab[authorize]' => TRUE,
      'create_new_job_tab[target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPost('node/1/edit', $edit, t('Create job'));
    $this->assertText(t('The node "test" has been sent to Smartling for translation to "nl".'));

    $submission = entity_load_single('smartling_entity_data', 1);
    $smartling_submission = drupal_container()->get('smartling.wrappers.smartling_submission_wrapper')
      ->setEntity($submission);

    $this->drupalGet('/admin/reports/dblog');
    $this->assertText(t('Smartling created a job'));
    $this->assertNoText(t('Smartling updated a job'));
    $this->assertText(t('Smartling created a batch'));
    $this->assertText(t('Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertNoText(t('Fallback: Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertText(t('Smartling executed a batch'));
  }

  /**
   * Create job async.
   */
  public function testNodeCreateJobAsync() {
    $edit = [
      'create_new_job_tab[name]' => 'Drupal classic connector test ' . $this->randomName(),
      'create_new_job_tab[description]' => 'Description ' . $this->randomString(),
      'create_new_job_tab[due_date][date]' => '12/12/2020',
      'create_new_job_tab[due_date][time]' => '12:12',
      'create_new_job_tab[authorize]' => TRUE,
      'create_new_job_tab[target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPost('node/1/edit', $edit, t('Create job'));
    $this->assertText(t('The node "test" has been queued for translation in Smartling to "nl". File will be uploaded to Smartling by next cron runs.'));

    $submission = entity_load_single('smartling_entity_data', 1);
    $smartling_submission = drupal_container()->get('smartling.wrappers.smartling_submission_wrapper')
      ->setEntity($submission);

    $this->drupalGet('/admin/reports/dblog');
    $this->assertText(t('Smartling created a job'));
    $this->assertNoText(t('Smartling updated a job'));
    $this->assertText(t('Smartling created a batch'));
    $this->assertNoText(t('Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertNoText(t('Smartling executed a batch'));

    $this->processQueues();

    $this->drupalGet('/admin/reports/dblog');
    $this->assertText(t('Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertNoText(t('Fallback: Smartling uploaded @filename', [
      '@filename' => $smartling_submission->getFileName(),
    ]));
    $this->assertText(t('Smartling executed a batch'));
  }

}
