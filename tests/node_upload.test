<?php

/**
 * @file
 * Tests for smartling.
 */
use Drupal\smartling\ApiWrapper\MockApiWrapper;

/**
 * SmartlingNodeUploadTest.
 */
class SmartlingNodeUploadTest extends SmartlingWebTestCase {

  /**
   * Test info.
   *
   * @return array
   *   Return test info.
   */
  public static function getInfo() {
    return array(
      'name' => 'Content - Upload content. Upload single node from Edit form',
      'description' => 'Test success flow of upload from Edit form',
      'group' => 'Smartling',
    );
  }

  /**
   * Test correct node upload.
   */
  public function testCorrectNodeUpload() {
    // Add predefined languages.
    locale_add_language('zh-hans');
    // Set correct account settings.
    $this->smartlingSetCorrectSettings();
    // Set nodes method for page CT.
    $this->smartlingSetMethodSettings('page');
    // Set title field for article CT.
    $this->smartlingSetFieldSettings('page', 'title_property_field');
    // Create node.
    $node_array = [
      'title' => 'Title test',
      'language' => 'en',
    ];
    $this->drupalPost('node/add/page', $node_array, t('Save'));

    // Send node to smartling.
    $edit = [
      'create_new_job_tab[name]' => 'Drupal classic connector test ' . $this->randomName(),
      'create_new_job_tab[description]' => 'Description ' . $this->randomString(),
      'create_new_job_tab[due_date][date]' => '12/12/2020',
      'create_new_job_tab[due_date][time]' => '12:12',
      'create_new_job_tab[authorize]' => TRUE,
      'create_new_job_tab[target][nl]' => 'nl',
      'smartling_users_time_zone' => 'Europe/Kiev',
    ];

    $this->drupalPost('node/1/edit', $edit, t('Create job'));
    $this->assertText(t('The node "@title" has been queued for translation in Smartling to "@langs".', [
      '@title' => 'Title test',
      '@langs' => 'nl',
    ]));

    $queue = db_select('queue', 'q')
      ->fields('q', ['data'])
      ->condition('q.name', 'smartling_upload', '=')
      ->execute()
      ->fetchObject();

    if (empty($queue)) {
      $this->error('Queue smartling_upload is empty.');
    }
    else {
      $data = unserialize($queue->data);
      $this->assertTrue(array_key_exists('eids', $data));
      $this->assertTrue(array_key_exists('job_id', $data));
      $this->assertTrue(array_key_exists('batch_uid', $data));
      $this->assertTrue(array_key_exists('execute_batch', $data));
      $this->assertEqual($data['eids'], ["1"]);
    }
  }

  /**
   * Test correct file export and download.
   */
  public function testNodeFlow() {
    /* @var $mockApiWrapper MockApiWrapper */
    $mockApiWrapper = drupal_container()->get('smartling.mock_api_wrapper');

    $mockApiWrapper->addExpectedFileForUpload(__DIR__ . '/xml_sources/node_upload__upload.xml');
    $mockApiWrapper->addExpectedFileForDownload(__DIR__ . '/xml_sources/node_upload__download.xml');
    $mockApiWrapper->addExpectedProgress(100);

    drupal_container()->set('smartling.api_wrapper', $mockApiWrapper);

    $settings = [
      'body' => [LANGUAGE_NONE => [['value' => 'Lorem Ipsum Body Text']]],
      'title' => 'Node title Lorem Ipsum',
    ];
    $node = $this->drupalCreateNode($settings);

    // Set correct account settings.
    $this->smartlingSetCorrectSettings();
    // Set nodes method for page CT.
    $this->smartlingSetMethodSettings('page');
    // Set title field for article CT.
    $this->smartlingSetFieldSettings('page', 'title_property_field');
    $this->smartlingSetFieldSettings('page', 'body');

    $smartling_data = smartling_create_from_entity($node, 'node', $node->language, 'nl');
    smartling_queue_send_to_translate_process([
      'eids' => [$smartling_data->eid],
      'batch_uid' => 'test_batch_uid',
      'job_id' => 'test_job_id',
      'execute_batch' => TRUE,
    ]);

    // @todo use API function for path building.
    $translation_file_path = DRUPAL_ROOT . '/' . $this->private_files_directory . '/smartling/' . strtolower(trim(preg_replace('#\W+#', '_', $node->title), '_')) . '_node_' . $node->nid . '.xml';

    $this->assertTrue(file_exists($translation_file_path));

    if (file_exists($translation_file_path)) {
      $translation_file = file_get_contents($translation_file_path);
      $assertion_file = file_get_contents(__DIR__ . '/xml_sources/node_upload__upload.xml');
      $this->assertEqual(trim($translation_file), trim($assertion_file));
    }
    // @todo validate xml structure and content.

    smartling_queue_download_translated_item_process($smartling_data->eid);

    //$smartling_data = smartling_entity_data_load($smartling_data->eid);
    //$this->assertEqual($this->private_files_directory, $smartling_data->translated_file_name . '   ||   ' . $smartling_data->file_name);
    $this->assertTrue(file_exists(DRUPAL_ROOT . '/' . $this->private_files_directory . '/smartling/' . strtolower(trim(preg_replace('#\W+#', '_', $node->title), '_')) . '_node_' . $node->nid . '_nl.xml'));

    drupal_static_reset('translation_node_get_translations');
    $translations = translation_node_get_translations(($node->tnid ?: $node->nid));

    $this->assertTrue(isset($translations['nl']));

    // @todo verify that everything was translated properly
  }

}
