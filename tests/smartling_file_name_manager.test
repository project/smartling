<?php

/**
 * @file
 * Tests for smartling.
 */
use Drupal\smartling\Utils\FileNameManager;
use Drupal\smartling\Wrappers\SmartlingEntityDataWrapper;

/**
 * FileNameManager test cases.
 */
class FileNameManagerTest extends SmartlingWebTestCase {

  /**
   * Get info.
   *
   * @return array
   *   Return info array.
   */
  public static function getInfo() {
    return [
      'name' => 'Smartling File name manager tests',
      'description' => 'Checks file name generation',
      'group' => 'Smartling',
    ];
  }

  public function testBuildFileNamePot() {
    $file_name_manager = new FileNameManager(
      [
        "smartling_interface_entity" => "gettext"
      ],
      drupal_container()->get('smartling.wrappers.drupal_api_wrapper')
    );

    $data = new stdClass();
    $data->entity_type = "smartling_interface_entity";
    $data->bundle = "article";
    $data->title = "Test TItlE";
    $data->rid = 42;

    $entity_data_wrapper = new SmartlingEntityDataWrapper(NULL);
    $entity_data_wrapper->setEntity($data);

    $file_name = $file_name_manager->buildFileName($entity_data_wrapper);

    $this->assertEqual($file_name, "smartling_interface_translation_article.pot");

    $translated_data = clone $data;
    $translated_data->file_name = $file_name;
    $translated_data->target_language = "fr";

    $entity_data_wrapper->setEntity($translated_data);

    $translated_file_name = $file_name_manager->buildTranslatedFileName($entity_data_wrapper);

    $this->assertEqual($translated_file_name, "smartling_interface_translation_article_fr.po");
  }

  public function testBuildFileNameTitleLessThan255() {
    $file_name_manager = new FileNameManager(
      [],
      drupal_container()->get('smartling.wrappers.drupal_api_wrapper')
    );

    $data = new stdClass();
    $data->entity_type = "node";
    $data->bundle = "article";
    $data->title = "Test TItlE";
    $data->rid = 42;

    $entity_data_wrapper = new SmartlingEntityDataWrapper(NULL);
    $entity_data_wrapper->setEntity($data);

    $file_name = $file_name_manager->buildFileName($entity_data_wrapper);

    $this->assertEqual(strlen($file_name), 22);
    $this->assertEqual($file_name, "test_title_node_42.xml");

    $translated_data = clone $data;
    $translated_data->file_name = $file_name;
    $translated_data->target_language = "fr";

    $entity_data_wrapper->setEntity($translated_data);

    $translated_file_name = $file_name_manager->buildTranslatedFileName($entity_data_wrapper);

    $this->assertEqual($translated_file_name, "test_title_node_42_fr.xml");
  }

  public function testBuildFileNameTitleEqualsTo255() {
    $file_name_manager = new FileNameManager(
      [],
      drupal_container()->get('smartling.wrappers.drupal_api_wrapper')
    );

    $data = new stdClass();
    $data->entity_type = "node";
    $data->bundle = "article";
    $data->title = "Node titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode title TEST";
    $data->rid = 42;

    $entity_data_wrapper = new SmartlingEntityDataWrapper(NULL);
    $entity_data_wrapper->setEntity($data);

    $file_name = $file_name_manager->buildFileName($entity_data_wrapper);

    $this->assertEqual(strlen($file_name), 255);
    $this->assertEqual($file_name_manager->buildFileName($entity_data_wrapper), "node_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenod_node_42.xml");

    $translated_data = clone $data;
    $translated_data->file_name = $file_name;
    $translated_data->target_language = "fr";

    $entity_data_wrapper->setEntity($translated_data);

    $translated_file_name = $file_name_manager->buildTranslatedFileName($entity_data_wrapper);

    $this->assertEqual($translated_file_name, "node_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_title_node_42_fr.xml");
    $this->assertEqual(strlen($translated_file_name), 255);
  }

  public function testBuildFileNameTitleMoreThan255() {
    $file_name_manager = new FileNameManager(
      [],
      drupal_container()->get('smartling.wrappers.drupal_api_wrapper')
    );

    $data = new stdClass();
    $data->entity_type = "node";
    $data->bundle = "article";
    $data->title = "Node titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode titleNode title TEST BIG TITLE";
    $data->rid = 42;

    $entity_data_wrapper = new SmartlingEntityDataWrapper(NULL);
    $entity_data_wrapper->setEntity($data);

    $file_name = $file_name_manager->buildFileName($entity_data_wrapper);

    $this->assertEqual(strlen($file_name), 255);
    $this->assertEqual($file_name_manager->buildFileName($entity_data_wrapper), "node_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenod_node_42.xml");

    $translated_data = clone $data;
    $translated_data->file_name = $file_name;
    $translated_data->target_language = "fr";

    $entity_data_wrapper->setEntity($translated_data);

    $translated_file_name = $file_name_manager->buildTranslatedFileName($entity_data_wrapper);

    $this->assertEqual($translated_file_name, "node_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_titlenode_title_node_42_fr.xml");
    $this->assertEqual(strlen($translated_file_name), 255);
  }
}
