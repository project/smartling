<?php
/**
 * @file
 * Contains callbacks for VBO actions.
 */

/**
 * Shared action for "create" and "add" actions.
 *
 * @param $entity
 * @param array $context
 */
function smartling_do_job_action($entity, array $context) {
  if (empty($context['job_id']) || empty($context['batch_uid'])) {
    return;
  }

  try {
    drupal_container()
      ->get('smartling.queue_managers.upload_router')
      ->routeUploadRequest(
        $context['entity_type'],
        $entity,
        $context['target_locales'],
        $context['job_id'],
        $context['batch_uid'],
        NULL,
        $context['progress']['current'] == $context['progress']['total']
      );
  } catch (\Drupal\smartling\SmartlingExceptions\SmartlingGenericException $e) {
    $entity_wrapper = entity_metadata_wrapper($context['entity_type'], $entity);
    $message = t("Sending translation for '@entity_type:@id' is failed. Type '@bundle' is not supported or has note been configured for translation. Please see recent log messages for more information.", [
      '@bundle' => $entity_wrapper->getBundle(),
      '@id' => $entity_wrapper->getIdentifier(),
      '@entity_type' => $context['entity_type'],
    ]);

    drupal_set_message($message, 'error');

    smartling_log_get_handler()->error($e->getMessage() . '   ' . $e->getTraceAsString());
  }
}

/**
 * Returns needed form.
 *
 * @param array $form
 * @param array $form_state
 * @param $form_service_name
 *
 * @return array
 */
function smartling_get_do_job_action_form(array $form, array &$form_state, $form_service_name) {
  try {
    $form = drupal_container()
      ->get($form_service_name)
      ->buildForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    return [];
  }

  return $form;
}

/**
 * Validates needed form.
 *
 * @param array $form
 * @param array $form_state
 * @param $form_service_name
 */
function smartling_do_job_action_form_validate(array $form, array &$form_state, $form_service_name) {
  try {
    drupal_container()
      ->get($form_service_name)
      ->validateForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
  }
}

/**
 * Submits needed form.
 *
 * @param array $form
 * @param array $form_state
 * @param $form_service_name
 *
 * @return array
 */
function smartling_do_job_action_form_submit(array $form, array &$form_state, $form_service_name) {
  $result = [];

  if (!smartling_is_configured()) {
    return $result;
  }

  try {
    $result = drupal_container()
      ->get($form_service_name)
      ->submitForm($form, $form_state);

    $count_op = count($form_state['selection']);
    smartling_log_get_handler()->info(
        format_string('Smartling Bulk Submit - add @count entities to queue', [
            "@count" => $count_op
        ])
    );
  }
  catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);

    $result = [];
  }

  return $result;
}

/**
 * Smartling do translate action.
 *
 * @param object $entity
 *   Action entity object.
 * @param array $context
 *   Context info.
 *
 * @global object $user
 *   Drupal user object.
 */
function smartling_do_translate_action($entity, array $context) {
  smartling_do_job_action($entity, $context);
}

/**
 * Smartling do translate action form.
 *
 * @param array $context
 *   Context info array.
 * @param array $form_state
 *   FAPI array.
 *
 * @return array
 *   Return FAPI array for smartling do translate action form.
 */
function smartling_do_translate_action_form(array $context, array &$form_state) {
  return smartling_get_do_job_action_form([], $form_state, 'smartling.forms.do_translate_action');
}

/**
 * Smartling do translate action form - Form Validate.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 */
function smartling_do_translate_action_validate(array $form, array &$form_state) {
  smartling_do_job_action_form_validate($form, $form_state, 'smartling.forms.do_translate_action');
}

/**
 * Smartling do translate action form - Form Submit.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 *
 * @return array
 *   Return array with languages to translate.
 */
function smartling_do_translate_action_submit(array $form, array $form_state) {
  return smartling_do_job_action_form_submit($form, $form_state, 'smartling.forms.do_translate_action');
}

/**
 * Smartling add to job action.
 *
 * @param object $entity
 *   Action entity object.
 * @param array $context
 *   Context info.
 *
 * @global object $user
 *   Drupal user object.
 */
function smartling_do_translate_add_to_job_action($entity, array $context) {
  smartling_do_job_action($entity, $context);
}

/**
 * Smartling add to job action form.
 *
 * @param array $context
 *   Context info array.
 * @param array $form_state
 *   FAPI array.
 *
 * @return array
 *   Return FAPI array for smartling do translate action form.
 */
function smartling_do_translate_add_to_job_action_form(array $context, array &$form_state) {
  return smartling_get_do_job_action_form([], $form_state, 'smartling.forms.do_translate_add_to_job_action');
}

/**
 * Smartling add to job action form - Form Validate.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 */
function smartling_do_translate_add_to_job_action_validate(array $form, array &$form_state) {
  smartling_do_job_action_form_validate($form, $form_state, 'smartling.forms.do_translate_add_to_job_action');
}

/**
 * Smartling add to job action form - Form Submit.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 *
 * @return array
 *   Return array with languages to translate.
 */
function smartling_do_translate_add_to_job_action_submit(array $form, array $form_state) {
  return smartling_do_job_action_form_submit($form, $form_state, 'smartling.forms.do_translate_add_to_job_action');
}

function smartling_do_download_translation_action(&$entity, $context) {
  smartling_queue_download_translated_item_process($entity->eid);
}

function smartling_do_resend_translation_action(&$entity, $context) {
  drupal_container()
    ->get('smartling.wrappers.smartling_submission_wrapper')
    ->setEntity($entity)
    ->setStatusByEvent(SMARTLING_STATUS_EVENT_SEND_TO_UPLOAD_QUEUE)
    ->save();
  drupal_container()
    ->get('smartling.queue_managers.upload')
    ->add(array($entity->eid));

  drupal_set_message(t('Entity "@title" has been queued for resubmission.', array('@title' => $entity->title)));
}

function smartling_do_cancel_action($entity, array $context) {
  drupal_container()
    ->get('smartling.wrappers.smartling_submission_wrapper')
    ->setEntity($entity)
    ->setStatus(SMARTLING_STATUS_PENDING_CANCEL)
    ->save();
}
