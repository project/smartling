<?php

/**
 * File with Smartling credentials for test project.
 *
 * Needed for Simpletests.
 */

define('SMARTLING_TEST_PROJECT_ID', 'SMARTLING_TEST_PROJECT_ID');
define('SMARTLING_USER_ID', 'SMARTLING_USER_ID');
define('SMARTLING_USER_SECRET', 'SMARTLING_USER_SECRET');
