<?php

/**
 * @file
 * Admin forms and other for Smartling module administration section.
 */

/*
 * Gets all the fields that are configured to be translated "by fields" and "by nodes".
 * That's a conflict situation that should be avoided.
 */
function smartling_get_translatable_fields_with_conflict_settings() {
  $fields_with_problem = array();
  $title_module_fields = array(
    'title_field',
    'name_field',
    'description_field'
  );

  $node_translate_fields = smartling_settings_get_handler()->nodeGetFieldsSettings();
  foreach ($node_translate_fields as $bundle => $fields) {
    // Error in field settings.
    foreach (field_info_instances('node', $bundle) as $field) {
      $field_machine_name = $field['field_name'];
      if (smartling_nodes_method($bundle) &&
          smartling_field_is_translatable($field_machine_name, 'node') &&
          !in_array($field_machine_name, $title_module_fields)
      ) {
        $fields_with_problem[$field_machine_name] = '<b>' . $field_machine_name . '</b>';
      }
    }
  }

  $term_translate_fields = smartling_settings_get_handler()->taxonomyTermGetFieldsSettings();
  foreach ($term_translate_fields as $bundle => $fields) {
    // Error in field settings.
    $voc = taxonomy_vocabulary_machine_name_load($bundle);
    $vocabulary_mode = i18n_taxonomy_vocabulary_mode($voc->vid);
    foreach (field_info_instances('taxonomy_term', $bundle) as $field) {
      $field_machine_name = $field['field_name'];
      if (($vocabulary_mode == I18N_MODE_TRANSLATE) &&
          smartling_field_is_translatable($field_machine_name, 'taxonomy_term') &&
          isset($term_translate_fields[$bundle][$field['field_name']]) &&
          !in_array($field_machine_name, $title_module_fields)
      ) {
        $fields_with_problem[$field_machine_name] = '<b>' . $field_machine_name . '</b>';
      }
    }
  }

  return $fields_with_problem;
}

/**
 * Smartling settings page callback.
 *
 * @return array
 *   Return render array.
 */
function smartling_admin_configuration_page() {
  // Add ajax library.
  drupal_add_library('system', 'drupal.ajax');
  $output['message'] = array(
    '#type' => 'markup',
    '#title' => 'Link to submission views',
    '#title_display' => 'invisible',
    '#prefix' => t('After you configure the Smartling module you can <a href="@url">submit content for translation</a>.', array('@url' => url('admin/content/smartling-content'))),
  );

  $output['smartling'] = array(
    '#type' => 'vertical_tabs',
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'smartling') . '/js/smartling_admin.js'),
      'css' => array(drupal_get_path('module', 'smartling') . '/css/smartling_admin.css'),
    ),
  );

  $settings_forms = module_invoke_all('smartling_settings_form_info');
  uasort($settings_forms, function ($a, $b) {
    return $a['weight'] > $b['weight'];
  });

  foreach ($settings_forms as $machine_name => $data) {
    $title = $data['title'];
    $form = drupal_get_form($machine_name);
    $output['smartling'][] = smartling_wrap_in_fieldset($form, $title);
  }

  $fields_with_problem = smartling_get_translatable_fields_with_conflict_settings();
  if (!empty($fields_with_problem) && is_array($fields_with_problem)) {
    $text = t('The following field(s) should be disabled for translation: !fields.', array('!fields' => implode(', ', $fields_with_problem)));
    $suffix = '<div id="smartling-untranslatable-fix">' . $text . '</div>';
    $output['fix'] = array(
      '#type' => 'markup',
      '#title' => t('Fix field settings'),
      '#title_display' => 'invisible',
      '#suffix' => $suffix,
    );
  }

  // Function smartling_get_dir() can return private file directory as well as
  // public. We suggest to use private file directory.
  $dir = smartling_get_dir();

  if (strpos($dir, 'private') === FALSE || !file_prepare_directory($dir)) {
    drupal_set_message(t("Please make sure that the <a href='@url'>Drupal private file system path</a> has been set up and the 'private://smartling' directory is writable by the web server. If 'private://smartling' directory does not exist then create it and make it writable.", [
      '@url' => url('/admin/config/media/file-system'),
    ]), 'error');
  }

  return $output;
}

/**
 * Form for smartling account settings.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 *
 * @return array
 *   Return FAPI array for smartling account settings.
 */
function smartling_admin_account_info_settings_form(array $form, array &$form_state) {
  try {
    $form = drupal_container()
      ->get('smartling.forms.admin_account_info_settings')
      ->buildForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
    return array();
  }
  return $form;
}

/**
 * Validate smartling account settings form.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 */
function smartling_admin_account_info_settings_form_validate(array $form, array &$form_state) {
  try {
    return drupal_container()
      ->get('smartling.forms.admin_account_info_settings')
      ->validateForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Smartling account settings - Form Submit.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 */
function smartling_admin_account_info_settings_form_submit(array $form, array &$form_state) {
  try {
    return drupal_container()
      ->get('smartling.forms.admin_account_info_settings')
      ->submitForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Form for smartling log settings.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 *
 * @return array
 *   Return FAPI array for smartling log settings.
 */
function smartling_admin_expert_settings_form(array $form, array &$form_state) {
  try {
    $form = drupal_container()
      ->get('smartling.forms.admin_expert_settings')
      ->buildForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
    return array();
  }
  return $form;
}

/**
 * Log Settings - Form Submit.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 */
function smartling_admin_expert_settings_form_submit(array $form, array &$form_state) {
  try {
    return drupal_container()
      ->get('smartling.forms.admin_expert_settings')
      ->submitForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Wrapper for most kind of entities.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 *
 * @return array
 *   Return FAPI array for smartling entity fields settings.
 */
function smartling_admin_entities_translation_settings_form(array $form, array &$form_state) {
  try {
    $form = drupal_container()
      ->get('smartling.forms.admin_entities_translation_settings')
      ->buildForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
    return array();
  }
  return $form;
}

/**
 * Custom submit handler for entities wrapper form.
 *
 * @see smartling_admin_entities_translation_settings_form()
 */
function smartling_admin_entities_translation_settings_form_submit(array $form, array &$form_state) {
  try {
    return drupal_container()
      ->get('smartling.forms.admin_entities_translation_settings')
      ->submitForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Form for smartling node fields settings.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 *
 * @return array
 *   Return FAPI array for smartling node fields settings.
 */
function smartling_admin_node_translation_settings_form(array $form, array &$form_state) {
  try {
    $form = drupal_container()
      ->get('smartling.forms.admin_node_translation_settings')
      ->buildForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
    return array();
  }
  return $form;
}

/**
 * Node Translation Settings - Form Submit.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 */
function smartling_admin_node_translation_settings_form_submit(array $form, array &$form_state) {
  try {
    return drupal_container()
      ->get('smartling.forms.admin_node_translation_settings')
      ->submitForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Form for smartling taxonomy fields settings.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 *
 * @return array
 *   Return FAPI array for smartling taxonomy fields settings.
 */
function smartling_admin_taxonomy_translation_settings_form(array $form, array &$form_state) {
  try {
    $form = drupal_container()
      ->get('smartling.forms.admin_taxonomy_translation_settings')
      ->buildForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
    return array();
  }
  return $form;
}

/**
 * Taxonomy Translation Settings - Form Submit.
 *
 * @param array $form
 *   FAPI array.
 * @param array $form_state
 *   FAPI array.
 */
function smartling_admin_taxonomy_translation_settings_form_submit(array $form, array &$form_state) {
  try {
    return drupal_container()
      ->get('smartling.forms.admin_taxonomy_translation_settings')
      ->submitForm($form, $form_state);
  } catch (\Exception $e) {
    watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Wrap form in fieldset.
 *
 * @param array $form
 *   FAPI array.
 * @param string $title
 *   Title for fieldset.
 *
 * @return array
 *   Return FAPI array.
 */
function smartling_wrap_in_fieldset(array $form, $title) {
  return array(
    '#type' => 'fieldset',
    '#group' => 'smartling',
    '#title' => $title,
    '#attributes' => array(
      'class' => array('smartling-' . strtolower(str_replace(' ', '-', $title))),
      'id' => array('smartling-' . strtolower(str_replace(' ', '-', $title))),
    ),
    'children' => $form,
  );
}

/**
 * Renders jobs listing page.
 */
function smartling_batches_listing_page() {
  // TODO 7.x-4.x: add paging, sorting and filtering when it's done in facade.
  $response = drupal_container()->get('smartling.api_wrapper')->listBatches();

  if (empty($response['items'])) {
    return [
      '#markup' => t('There are no active operations'),
    ];
  }

  $header = [
    ['data' => t('Error')],
    ['data' => t('Operation status')],
    ['data' => t('Job name')],
    ['data' => t('Job uid')],
    ['data' => t('Operation started')],
    ['data' => t('Operation ended')],
    ['data' => t('Operation info')],
  ];
  $rows = [];

  foreach ($response['items'] as $batch) {
    $rows[] = [
      'data' => [
        'error' => [
          'data' => $batch['status'] != 'Completed' ? '<img src="/' . drupal_get_path('module', 'smartling') . '/img/throbber.gif">' : '<div></div>',
          'class' => 'column-error status-icon',
        ],
        'status' => [
          'data' => $batch['status'],
          'class' => 'column-status',
        ],
        'job_name' => [
          'data' => '<img src="/' . drupal_get_path('module', 'smartling') . '/img/throbber.gif">',
          'class' => 'column-job-name',
          'job-uid' => $batch['translationJobUid'],
        ],
        'job_uid' => [
          'data' => $batch['translationJobUid'],
          'class' => 'column-job-uid',
        ],
        'batch_created_date' => [
          'data' => date('Y-m-d H:i:s', strtotime($batch['createdDate'])),
          'class' => 'column-batch-created-date',
        ],
        'batch_modified_date' => [
          'data' => date('Y-m-d H:i:s', strtotime($batch['modifiedDate'])),
          'class' => 'column-batch-modified-date',
        ],
        'batch_link' => [
          'data' => l(t('More info'), '/admin/config/regional/smartling/batch-list/' . $batch['batchUid'], [
            'attributes' => [
              'target' => '_blank',
            ],
          ]),
          'class' => 'column-batch-link',
        ],
      ],
      'class' => [
        $batch['hasError'] ? 'error' : 'ok',
        $batch['status'] == 'Completed' ? 'batch-completed' : 'batch-not-completed',
      ],
      'batch-uid' => $batch['batchUid'],
    ];
  }

  $page = [
    '#markup' => theme('table', [
      'header' => $header,
      'rows' => $rows,
      'attributes' => [
        'class' => [
          'system-status-report',
        ],
      ],
    ]),
    '#attached' => [
      'css' => [
        drupal_get_path('module', 'smartling') . '/css/smartling_jobs.css' => [
          'type' => 'file',
        ],
      ],
      'js' => [
        drupal_get_path('module', 'smartling') . '/js/smartling_jobs.js',
      ],
    ],
  ];

  return $page;
}

/**
 * Renders batch status page.
 */
function smartling_batch_status_page($batch_uid) {
  $response = drupal_container()->get('smartling.api_wrapper')->getBatchStatus($batch_uid);

  if (empty($response)) {
    return [
      '#markup' => t('Information not found'),
    ];
  }

  $general_errors = json_decode($response['generalErrors'], TRUE);

  $files = [];

  // Files table.
  foreach ($response['files'] as $file) {
    $target_locales = [];
    $errors = json_decode($file['errors'], TRUE);

    foreach ($file['targetLocales'] as $item) {
      $target_locales[] = $item['localeId'] . ' (added:' . $item['stringsAdded'] . ', skipped: ' . $item['stringsSkipped'] . ')';
    }

    $files[] = [
      'data' => [
        'file_uri' => [
          'data' => $file['fileUri'],
        ],
        'status' => [
          'data' => $file['status'],
        ],
        'target_locales' => [
          'data' => theme_item_list([
            'items' => $target_locales,
            'type' => 'ul',
            'title' => NULL,
            'attributes' => [],
          ]),
        ],
        'updated_date' => [
          'data' => date('Y-m-d H:i:s', strtotime($file['updatedDate'])),
        ],
        'error' => [
          'data' => empty($errors['detailMessage']) ? t('No errors') : $errors['cause']['detailMessage'] . ': ' . $errors['detailMessage'],
        ],
      ],
    ];
  }

  $files_table = empty($files) ? t('There are no attached files yet.') : theme('table', [
    'header' => [
      t('File uri'),
      t('File status'),
      t('Target locales'),
      t('Updated date'),
      t('Errors'),
    ],
    'rows' => $files,
  ]);

  // Batch table.
  $rows = [
    [
      ['data' => t('Job uid'), 'header' => TRUE],
      $response['jobUid'],
    ],
    [
      ['data' => t('Status'), 'header' => TRUE],
      $response['status'],
    ],
    [
      ['data' => t('Updated date'), 'header' => TRUE],
      date('Y-m-d H:i:s', strtotime($response['updatedDate'])),
    ],
    [
      ['data' => t('General errors'), 'header' => TRUE],
      empty($general_errors['detailMessage']) ? t('No errors') : $general_errors['cause']['detailMessage'] . ': ' . $general_errors['detailMessage'],
    ],
    [
      ['data' => t('Files'), 'header' => TRUE],
      $files_table
    ],
  ];

  $build['batch_table'] = [
    '#theme' => 'table',
    '#rows' => $rows,
  ];

  return $build;
}

/**
 * Returns job info.
 */
function smartling_get_job_callback($job_uid) {
  echo json_encode(drupal_container()->get('smartling.api_wrapper')->getJob($job_uid));
}

/**
 * Returns batch info.
 */
function smartling_get_batches_callback() {
  // TODO 7.x-4.x: add paging, sorting and filtering when it's done in facade.
  echo json_encode(drupal_container()->get('smartling.api_wrapper')->listBatches());
}
