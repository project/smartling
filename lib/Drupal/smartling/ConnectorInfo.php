<?php

/**
 * @file
 * ConnectorInfo.php.
 */

namespace Drupal\smartling;

use Smartling\BaseApiAbstract;

/**
 * Class ConnectorInfo
 */
class ConnectorInfo {

  /**
   * Returns module name.
   *
   * @return string
   */
  public static function getLibName() {
    return 'drupal-classic-connector';
  }

  /**
   * Returns module version.
   *
   * @param string $name
   * @param string $default
   * @return string
   */
  public static function getLibVersion($name = 'smartling', $default = '7.x-4.x-dev') {
    $info = system_get_info('module', $name);
    $client_version = $default;

    if (!empty($info['version'])) {
      $client_version = $info['version'];
    }

    return $client_version;
  }

  /**
   * Set up current client id and version.
   */
  public static function setUpCurrentClientInfo() {
    BaseApiAbstract::setCurrentClientId(self::getLibName());
    BaseApiAbstract::setCurrentClientVersion(self::getLibVersion());
  }

}
