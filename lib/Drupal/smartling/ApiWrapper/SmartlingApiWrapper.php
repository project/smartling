<?php

/**
 * @file
 * Facade for Smartling APIs.
 */

namespace Drupal\smartling\ApiWrapper;

use DateTime;
use DateTimeZone;
use Drupal\smartling\ApiWrapperInterface;
use Drupal\smartling\ConnectorInfo;
use Drupal\smartling\Log\SmartlingLog;
use Drupal\smartling\Settings\SmartlingSettingsHandler;
use Exception;
use HtmlSaveComplete;
use InvalidArgumentException;
use Smartling\BaseApiAbstract;
use Smartling\Batch\BatchApi;
use Smartling\Batch\Params\CreateBatchParameters;
use Smartling\Context\ContextApi;
use Smartling\Context\Params\MatchContextParameters;
use Smartling\Context\Params\UploadContextParameters;
use Smartling\Context\Params\UploadResourceParameters;
use Smartling\Exceptions\SmartlingApiException;
use Smartling\File\FileApi;
use Smartling\File\Params\DownloadFileParameters;
use Smartling\File\Params\UploadFileParameters;
use Smartling\Jobs\JobsApi;
use Smartling\Jobs\Params\CancelJobParameters;
use Smartling\Jobs\Params\CreateJobParameters;
use Smartling\Jobs\Params\ListJobsParameters;
use Smartling\Jobs\Params\UpdateJobParameters;
use Smartling\Project\ProjectApi;

/**
 * Class SmartlingApiWrapper.
 */
class SmartlingApiWrapper implements ApiWrapperInterface {

  /**
   * @var SmartlingSettingsHandler
   */
  protected $settingsHandler;

  /**
   * @var SmartlingLog
   */
  protected $logger;

  /**
   * @var FileApi
   */
  protected $api;

  /**
   * @var ProjectApi
   */
  protected $projectApi;

  /**
   * @var ContextApi
   */
  protected $contextApi;

   /**
    * @var JobsApi
    */
   protected $jobsApi;

  /**
   * @var BatchApi
   */
  protected $batchApi;

  /**
   * This function converts Drupal locale to Smartling locale.
   *
   * @param string $locale
   *   Locale string in some format: 'en' or 'en-US'.
   *
   * @return string|null
   *   Return locale or NULL.
   */
  protected function convertLocaleDrupalToSmartling($locale) {
    return smartling_convert_locale_drupal_to_smartling($locale);
  }

  /**
   * This function converts Smartling locale to Drupal locale.
   *
   * @param string $locale
   *   Locale string in some format: 'en' or 'en-US'.
   *
   * @return string|null
   *   Return locale or NULL.
   */
  protected function convertLocaleSmartlingToDrupal($locale) {
    return smartling_convert_locale_smartling_to_drupal($locale);
  }
  /**
   * Initialize.
   *
   * @param SmartlingSettingsHandler $settings_handler
   * @param SmartlingLog $logger
   */
  public function __construct(SmartlingSettingsHandler $settings_handler, SmartlingLog $logger) {
    $this->settingsHandler = $settings_handler;
    $this->logger = $logger;
    $api_factory = drupal_container()->get('smartling.smartling_api_factory');

    $this->setApi($api_factory::create($this->settingsHandler, 'project'), 'project');
    $this->setApi($api_factory::create($this->settingsHandler, 'context'), 'context');
    $this->setApi($api_factory::create($this->settingsHandler, 'jobs'), 'jobs');
    $this->setApi($api_factory::create($this->settingsHandler, 'batch'), 'batch');
    $this->setApi($api_factory::create($this->settingsHandler));
  }

  /**
   * {@inheritdoc}
   *
   * We don't use type hint as BaseApiAbstract because we have
   * SmartlingContextAPI class that isn't extended from BaseApiAbstract.
   */
  public function setApi($api, $api_type = 'file') {
    switch ($api_type) {
      case 'file':
        $this->api = $api;

        break;

      case 'project':
        $this->projectApi = $api;

        break;

       case 'jobs':
       $this->jobsApi = $api;

       break;

      case 'batch':
        $this->batchApi = $api;

        break;

      case 'context':
        $this->contextApi = $api;

        break;
    }
  }

  // TODO: 3.x - never used?
  public function getLocaleList() {
    $response = $this->api->getLocaleList();
    $response = json_decode($response);
    $locales = isset($response->response->data->locales) ? $response->response->data->locales : array();
    $result = array();
    foreach ($locales as $locale) {
      $result[$locale->locale] = "{$locale->name} ({$locale->translated})";
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function downloadFile($smartling_entity) {
    $smartling_entity_type = $smartling_entity->entity_type;
    $d_locale = $smartling_entity->target_language;
    $file_name_unic = $smartling_entity->file_name;

    $retrieval_type = $this->settingsHandler->variableGet('smartling_retrieval_type', 'published');

    $download_param = new DownloadFileParameters();
    $download_param->setRetrievalType($retrieval_type);

    $this->logger->info("Smartling queue start download '@file_name' file and update fields for @entity_type id - @rid, locale - @locale.",
      array(
        '@file_name' => $file_name_unic,
        '@entity_type' => $smartling_entity_type,
        '@rid' => $smartling_entity->rid,
        '@locale' => $smartling_entity->target_language
      ));

    $s_locale = $this->convertLocaleDrupalToSmartling($d_locale);

    // Try to download file.
    try {
      $download_result = (string) $this->api->downloadFile($file_name_unic, $s_locale, $download_param);
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('smartling_queue_download_translated_item_process tried to download file:<br/>
      Project Id: @project_id <br/>
      Action: download <br/>
      URI: @file_uri <br/>
      Drupal Locale: @d_locale <br/>
      Smartling Locale: @s_locale <br/>
      Error: @message',
        array(
          '@project_id' => $this->settingsHandler->getProjectId(),
          '@file_uri' => $file_name_unic,
          '@d_locale' => $d_locale,
          '@s_locale' => $s_locale,
          '@message' => $e->getMessage(),
        ), TRUE);

      $download_result = FALSE;
    }

    return $download_result;
  }


  /**
   * {@inheritdoc}
   */
  public function getStatus($smartling_entity) {
    $error_result = NULL;

    if ($smartling_entity === FALSE) {
      $this->logger->error(
        'Smartling checks status for id - @rid is FAIL! Smartling entity not exist.',
        array('@rid' => $smartling_entity->rid),
        TRUE
      );
      return $error_result;
    }

    $file_name_unic = $smartling_entity->file_name;

    $s_locale = $this->convertLocaleDrupalToSmartling($smartling_entity->target_language);

    // Try to retrieve file status.
    try {
      $status_result = $this->api->getStatus($file_name_unic, $s_locale);
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('Smartling checks status for @entity_type id - @rid: <br/>
      Project Id: @project_id <br/>
      Action: status <br/>
      URI: @file_uri <br/>
      Drupal Locale: @d_locale <br/>
      Smartling Locale: @s_locale <br/>
      Error: @message', array(
        '@entity_type' => $smartling_entity->entity_type,
        '@rid' => $smartling_entity->rid,
        '@project_id' => $this->settingsHandler->getProjectId(),
        '@file_uri' => $file_name_unic,
        '@d_locale' => $smartling_entity->target_language,
        '@s_locale' => $s_locale,
        '@message' => $e->getMessage(),
      ), TRUE);

      return $error_result;
    }

    $this->logger->info('Smartling checks status for @entity_type id - @rid (@d_locale). approvedString = @as, completedString = @cs',
      array(
        '@entity_type' => $smartling_entity->entity_type,
        '@rid' => $smartling_entity->rid,
        '@d_locale' => $smartling_entity->target_language,
        '@as' => $status_result['authorizedStringCount'],
        '@cs' => $status_result['completedStringCount'],
      ));

    // If true, file translated.
    $authorized = $status_result['authorizedStringCount'];
    $completed = $status_result['completedStringCount'];
    $progress = ($authorized + $completed > 0) ? (int) (($completed / ($authorized + $completed)) * 100) : 0;
    $smartling_entity->download = 0;
    $smartling_entity->progress = $progress;
    $smartling_entity->status = SMARTLING_STATUS_IN_TRANSLATE;

    return array(
      'entity_data' => $smartling_entity,
      'response_data' => $status_result,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function testConnection(array $locales) {
    $result = array();

    try {
      $project_details = $this->projectApi->getProjectDetails();
      $target_locales = [];

      foreach ($project_details['targetLocales'] as $targetLocale) {
        if (!empty($targetLocale['enabled'])) {
          $target_locales[] = $targetLocale['localeId'];
        }
      }

      foreach ($locales as $key => $locale) {
        if ($locale !== 0 && $locale == $key) {
          $s_locale = $this->convertLocaleDrupalToSmartling($locale);
          $result[$s_locale] = FALSE;

          if (in_array($s_locale, $target_locales)) {
            $result[$s_locale] = TRUE;
          }
        }
      }
    }
    catch (SmartlingApiException $e) {
      $this->logger->warning('Connection test for project: @project_id FAILED.',
        array(
          '@project_id' => $this->settingsHandler->getProjectId(),
        ));
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  // TODO : Replace $file_type with enum class
  public function uploadFile($file_path, $file_name_unic, $file_type, array $locales, $batch_uid) {
    $locales_to_approve = [];

    foreach ($locales as $locale) {
      $locales_to_approve[] = $this->convertLocaleDrupalToSmartling($locale);
    }

    // Handle "Many Drupal locales -> One Smartling locale" mapping.
    $locales_to_approve = array_unique($locales_to_approve);

    $upload_params = new UploadFileParameters();
    $upload_params->setClientLibId(
      BaseApiAbstract::getCurrentClientId(),
      BaseApiAbstract::getCurrentClientVersion()
    );

    // Fallback behavior for previous implementation: if batch_uid is empty
    // (NULL) then we should upload file through File API and authorize content
    // only if this option is enabled. If not empty then upload file through
    // Batch API.
    if (!empty($batch_uid)) {
      $upload_params->setLocalesToApprove($locales_to_approve);
    }
    elseif ($this->settingsHandler->getAutoAuthorizeContent()) {
      $upload_params->setLocalesToApprove($locales_to_approve);
    }

    if ($this->settingsHandler->getCallbackUrlUse()) {
      $upload_params->setCallbackUrl($this->settingsHandler->getCallbackUrl());
    }

    if ($this->settingsHandler->getOverwriteApprovedLocales()) {
      $upload_params->set('overwriteAuthorizedLocales', 1);
    }

    try {
      // Fallback behavior for previous implementation: if batch_uid is empty
      // (NULL) then we should upload file through File API. If not empty then
      // upload file through Batch API.
      if (!empty($batch_uid)) {
        $this->batchApi->uploadBatchFile($file_path, $file_name_unic, $file_type, $batch_uid, $upload_params);
        $message = 'Smartling uploaded @file_name for Drupal locales: @locales';
      }
      else {
        $this->api->uploadFile($file_path, $file_name_unic, $file_type, $upload_params);
        $message = 'Fallback: Smartling uploaded @file_name for Drupal locales: @locales';
      }

      $this->logger->info($message, [
        '@file_name' => $file_name_unic,
        '@locales' => implode('; ', $locales),
        'entity_link' => l(t('View file'), $file_path)
      ]);

      return SMARTLING_STATUS_EVENT_UPLOAD_TO_SERVICE;
    }
    catch (SmartlingApiException $e) {
      if (!empty($batch_uid)) {
        $message = 'Smartling failed to upload xml file: <br/>
          Project Id: @project_id <br/>
          Action: upload <br/>
          URI: @file_uri <br/>
          Drupal Locale: @d_locale <br/>
          Error: @message <br/>
          Upload params: @upload_params';
      }
      else {
        $message = 'Fallback: Smartling failed to upload xml file: <br/>
          Project Id: @project_id <br/>
          Action: upload <br/>
          URI: @file_uri <br/>
          Drupal Locale: @d_locale <br/>
          Error: @message <br/>
          Upload params: @upload_params';
      }

      $this->logger->error($message, [
        '@project_id' => $this->settingsHandler->getProjectId(),
        '@file_uri' => $file_path,
        '@d_locale' => implode('; ', $locales),
        '@message' => $e->getMessage(),
        '@upload_params' => print_r($upload_params->exportToArray(), TRUE),
      ], TRUE);
    }

    return SMARTLING_STATUS_EVENT_FAILED_UPLOAD;
  }

  /**
   * Uploads context file to Smartling and writes some logs.
   *
   * @param array $data
   * @return int
   */
  public function uploadContext($data) {
    $data['action'] = 'upload';
    $result = FALSE;

    try {
      $context_file_name = str_replace(['/', '=', '.'], '_', implode('_', parse_url($data['url']))) . '.html';
      $path = $this->settingsHandler->getDir() . '/smartling_context/' . $context_file_name;
      $dirname = dirname($path);

      if (file_prepare_directory($dirname, FILE_CREATE_DIRECTORY) && ($file = file_save_data($data['html'], $path, FILE_EXISTS_REPLACE))) {
        $path = drupal_realpath($file->uri);

        $match_params = new MatchContextParameters();
        $match_params->setContentFileUri($data['file_name']);
        $match_params->setOverrideContextOlderThanDays(0);

        $upload_params = new UploadContextParameters();
        $upload_params->setContent($path);
        $upload_params->setName($data['url']);

        $upload_params_with_matching = new UploadContextParameters();
        $upload_params_with_matching->setContent($path);
        $upload_params_with_matching->setName($data['url']);
        $upload_params_with_matching->setMatchParams($match_params);

        $this->contextApi->uploadAndMatchContextSync($upload_params_with_matching);
        $this->contextApi->uploadAndMatchContext($upload_params);

        $this->uploadContextMissingResources($dirname, [
          'basic_auth' => [
            'enabled' => variable_get('smartling_translation_context_basic_auth_enabled', FALSE),
            'login' => variable_get('smartling_translation_context_basic_auth_login', ''),
            'password' => variable_get('smartling_translation_context_basic_auth_password', ''),
          ]
        ]);

        $result = TRUE;
      }
      else {
        $this->logger->error("Can't save context file: @path", [
          '@path' => $path,
        ]);
      }
    }
    catch (SmartlingApiException $e) {
      $result = FALSE;
      $this->logger->error('Smartling failed to upload context for @url with message: @message', array(
        '@url' => $data['url'],
        '@message' => $e->getMessage()
      ), TRUE);
    }

    return $result;
  }

  /**
   * @param $smartling_context_directory
   * @param array $settings
   */
  protected function uploadContextMissingResources($smartling_context_directory, array $settings) {
    // Cache for resources which we can't upload. Do not try to re-upload them
    // for 1 hour. After 1 hour cache will be reset and we will try again.
    $cache_name = 'smartling_context_resources_cache';
    $time_to_live = 60 * 60;
    $two_days = 2 * 24 * 60 * 60;
    $cache = cache_get($cache_name);
    $cached_data = empty($cache) ? [] : $cache->data;
    $update_cache = FALSE;
    $smartling_context_resources_directory = $smartling_context_directory . '/resources';
    $asset_inliner = new HtmlSaveComplete();

    // Do nothing if directory for resources isn't accessible.
    if (!file_prepare_directory($smartling_context_resources_directory, FILE_CREATE_DIRECTORY)) {
      $this->logger->error("Context resources directory @dir doesn't exist or is not writable. Missing resources were not uploaded. Context might look incomplete.", [
        '@dir' => $smartling_context_directory,
      ]);

      return;
    }

    try {
      $time_out = drupal_is_cli() ? 300 : 30;
      $start_time = time();

      do {
        $delta = time() - $start_time;

        if ($delta > $time_out) {
          throw new SmartlingApiException(vsprintf('Not all context resources are uploaded after %s seconds.', [$delta]));
        }

        $all_missing_resources = $this->contextApi->getAllMissingResources();

        // Method getAllMissingResources can return not all missing resources
        // in case it took to much time. Log this information.
        if (!$all_missing_resources['all']) {
          $this->logger->warning('Not all missing context resources are received. Context might look incomplete.');
        }

        $fresh_resources = [];

        foreach ($all_missing_resources['items'] as $item) {
          if (!in_array($item['resourceId'], $cached_data)) {
            $fresh_resources[] = $item;
          }
        }

        // Walk through missing resources and try to upload them.
        foreach ($fresh_resources as $item) {
          if ((time() - strtotime($item['created'])) >= $two_days) {
            $update_cache = TRUE;
            $cached_data[] = $item['resourceId'];

            continue;
          }

          // If current resource isn't in the cache and it's accessible then
          // it means we can try to upload it.
          if (!in_array($item['resourceId'], $cached_data) && $asset_inliner->remote_file_exists($item['url'], $settings)) {
            $smartling_context_resource_file = $smartling_context_resources_directory . '/' . $item['resourceId'];
            $smartling_context_resource_file_content = $asset_inliner->getUrlContents($settings, $item['url']);

            // Ensure that resources directory is accessible, resource
            // downloaded properly and only then upload it. ContextAPI will not
            // be able to fopen() resource which is behind basic auth. So
            // download it first (with a help of curl), save it to smartling's
            // directory and then upload.
            if ($file = file_save_data($smartling_context_resource_file_content, $smartling_context_resource_file, FILE_EXISTS_REPLACE)) {
              $path = drupal_realpath($file->uri);
              $params = new UploadResourceParameters();
              $params->setFile($path);
              $is_resource_uploaded = $this->contextApi->uploadResource($item['resourceId'], $params);

              // Resource isn't uploaded for some reason. Log this info and set
              // resource id into the cache. We will not try to upload this
              // resource for the next hour.
              if (!$is_resource_uploaded) {
                $update_cache = TRUE;
                $cached_data[] = $item['resourceId'];

                $this->logger->warning("Can't upload context resource file with id = @id and url = @url. Context might look incomplete.", [
                  '@id' => $item['resourceId'],
                  '@url' => $item['url'],
                ]);
              }
            }
            // We can't save context resource file. Log this info.
            else {
              $this->logger->error("Can't save context resource file: @path", [
                '@path' => $smartling_context_resource_file,
              ]);
            }
          }
          else {
            // Current resource isn't accessible (or already in the cache).
            // If first case then add inaccessible resource into the cache.
            if (!in_array($item['resourceId'], $cached_data)) {
              $update_cache = TRUE;
              $cached_data[] = $item['resourceId'];

              $this->logger->warning("File @file can not be downloaded.", [
                '@file' => $item['url'],
              ]);
            }
          }
        }

        // Set failed resources into the cache for the next hour.
        if ($update_cache) {
          cache_set($cache_name, $cached_data, 'cache', time() + $time_to_live);
        }
      } while (!empty($fresh_resources));
    } catch (Exception $e) {
      watchdog_exception('smartling', $e);
    }
  }

  public function deleteFile($file_name) {
    try {
      $this->api->deleteFile($file_name);
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('Smartling failed to delete file: @file_name', array('@file_name' => $file_name), TRUE);

      return -1;
    }

    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusAllLocales($file_name) {
    $error_result = NULL;

    if (empty($file_name)) {
      //@todo: improve log message.
      $this->logger->error(
        'Smartling checks status for file - @file_name failed! Because it was empty.',
        array('@file_name' => $file_name),
        TRUE
      );
      return $error_result;
    }

    // Try to retrieve file status.
    try {
      $status_result = $this->api->getStatusAllLocales($file_name);
    }
    catch (SmartlingApiException $e) {
      if (!empty($e->getErrorsByKey('file.not.found'))) {
        throw $e;
      }

      $this->logger->error('Smartling checks status for file name: @file_uri <br/>
      Project Id: @project_id <br/>
      Action: status <br/>
      Error: @message', array(
        '@project_id' => $this->settingsHandler->getProjectId(),
        '@file_uri' => $file_name,
        '@message' => $e->getMessage(),
      ), TRUE);

      return $error_result;
    }

    $this->logger->info('Smartling checks status for file: @file_name. totalString = @as',
      array(
        '@file_name' => $file_name,
        '@as' => $status_result['totalStringCount'],
      ));

    $result = array();
    foreach ($status_result['items'] as $item) {
      $locales = $this->convertLocaleSmartlingToDrupal($item['localeId']);

      foreach ($locales as $locale) {
        $result[$locale] = (array) $item;
      }
    }

    return $result;
  }

  public function getLastModified($file_name) {
    //return $this->api->getLastModified($fileUri, $locale, $params);

    $error_result = NULL;

    if (empty($file_name)) {
      //@todo: improve log message.
      $this->logger->error('Smartling check of last updated date for file - @file_name failed.', array('@file_name' => $file_name), TRUE);
      return $error_result;
    }

    // Try to retrieve file status.
    try {
      $status_result = $this->api->getLastModified($file_name);
    }
    catch (SmartlingApiException $e) {
      if (!empty($e->getErrorsByKey('file.not.found'))) {
        throw $e;
      }

      $this->logger->error('Smartling check last updated dates for file name: @file_uri <br/>
      Project Id: @project_id <br/>
      Action: status <br/>
      Error: @message', array(
        '@project_id' => $this->settingsHandler->getProjectId(),
        '@file_uri' => $file_name,
        '@message' => $e->getMessage(),
      ), TRUE);
      return $error_result;
    }

    $result = array();
    foreach ($status_result['items'] as $item) {
      $locales = $this->convertLocaleSmartlingToDrupal($item['localeId']);

      foreach ($locales as $locale) {
        $result[$locale] = strtotime($item['lastModified']);
      }
    }

    return $result;
  }

  public function createJob($name, $description, DateTime $due_date = NULL, array $locales = []) {
    $result = [
      'job_id' => NULL,
      'errors' => [],
    ];

    try {
      $params = new CreateJobParameters();
      $params->setName($name);
      $params->setDescription($description);

      if (!empty($due_date)) {
        $params->setDueDate($due_date);
      }

      $response = $this->jobsApi->createJob($params);
      $result['job_id'] = $response['translationJobUid'];

      $this->logger->info('Smartling created a job:<br/>
      Id: @id<br/>
      Name: @name<br/>
      Description: @description<br/>
      Due date (UTC): @due_date<br/>', [
        '@id' => $result['job_id'],
        '@name' => $name,
        '@description' => $description,
        '@due_date' => empty($due_date) ? '' : $due_date->format('Y-m-d H:i'),
      ]);
    }
    catch (SmartlingApiException $e) {
      foreach ($e->getErrors() as $error) {
        $result['errors'][] = $error['message'];
      }
    }
    catch (Exception $e) {
      $this->logger->error('Smartling failed to create a job:<br/>
      Name: @name<br/>
      Description: @description<br/>
      Due date (UTC): @due_date<br/>
      Error: @error', [
        '@name' => $name,
        '@description' => $description,
        '@due_date' => empty($due_date) ? '' : $due_date->format('Y-m-d H:i'),
        '@error' => $e->getMessage(),
      ]);

      $result['errors'][] = $e->getMessage();
    }

    return $result;
  }

  public function updateJob($job_id, $name, $description = NULL, DateTime $due_date = NULL) {
    $result = [
      'job_id' => NULL,
      'errors' => [],
    ];

    try {
      $params = new UpdateJobParameters();
      $params->setName($name);

      if (!empty($description)) {
        $params->setDescription($description);
      }

      if (!empty($due_date)) {
        $params->setDueDate($due_date);
      }

      $response = $this->jobsApi->updateJob($job_id, $params);
      $result['job_id'] = $response['translationJobUid'];

      $this->logger->info('Smartling updated a job:<br/>
      Id: @id<br/>
      Name: @name<br/>
      Description: @description<br/>
      Due date (UTC): @due_date<br/>', [
        '@id' => $job_id,
        '@name' => $name,
        '@description' => $description,
        '@due_date' => empty($due_date) ? '' : $due_date->format('Y-m-d H:i'),
      ]);
    }
    catch (SmartlingApiException $e) {
      foreach ($e->getErrors() as $error) {
        $result['errors'][] = $error['message'];
      }
    }
    catch (Exception $e) {
      $this->logger->error('Smartling failed to update a job:<br/>
      Id: @id<br/>
      Name: @name<br/>
      Description: @description<br/>
      Due date (UTC): @due_date<br/>
      Error: @error', [
        '@id' => $job_id,
        '@name' => $name,
        '@description' => $description,
        '@due_date' => empty($due_date) ? '' : $due_date->format('Y-m-d H:i'),
        '@error' => $e->getMessage(),
      ]);

      $result['errors'][] = $e->getMessage();
    }

    return $result;
  }

  public function listJobs($name = NULL, array $statuses = []) {
    $result = [];

    try {
      $params = new ListJobsParameters();

      if (!empty($name)) {
        $params->setName($name);
      }

      if (!empty($statuses)) {
        $params->setStatuses($statuses);
      }

      $result = $this->jobsApi->listJobs($params);
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('Smartling failed to fetch list of available jobs:<br/>
      Error: @error', [
          '@error' => $e->getMessage(),
        ]
      );
    }

    return $result;
  }

  public function getJob($job_id) {
    $result = [];

    try {
      $result = $this->jobsApi->getJob($job_id);
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('Smartling failed to fetch a job:<br/>
      Job id: @job_id
      Error: @error', [
          '@job_id' => $job_id,
          '@error' => $e->getMessage(),
        ]
      );
    }

    return $result;
  }

  public function cancelJob($job_id) {
    $result = [];

    try {
      $params = new CancelJobParameters();
      $params->setReason('It was test job');
      $result = $this->jobsApi->cancelJobSync($job_id, $params);
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('Smartling failed to cancel a job:<br/>
      Job id: @job_id
      Error: @error', [
          '@job_id' => $job_id,
          '@error' => $e->getMessage(),
        ]
      );
    }

    return $result;
  }

  public function createBatch($job_id, $authorize) {
    $result = [
      'batch_uid' => NULL,
      'errors' => [],
    ];

    try {
      $params = new CreateBatchParameters();
      $params->setTranslationJobUid($job_id);
      $params->setAuthorize($authorize);
      $response = $this->batchApi->createBatch($params);
      $result['batch_uid'] = $response['batchUid'];

      $this->logger->info('Smartling created a batch:<br/>Id: @id', [
        '@id' => $result['batch_uid'],
      ]);
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('Smartling failed to create a batch:<br/>Error: @error', [
        '@error' => $e->getMessage(),
      ]);

      foreach ($e->getErrors() as $error) {
        $result['errors'][] = $error['message'];
      }
    }

    return $result;
  }

  public function executeBatch($batch_uid) {
    $result = FALSE;

    try {
      $this->batchApi->executeBatch($batch_uid);

      $this->logger->info('Smartling executed a batch:<br/>Id: @id', [
        '@id' => $batch_uid,
      ]);
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('Smartling failed to execute a batch @batch_uid:<br/>Error: @error', [
        '@batch_uid' => $batch_uid,
        '@error' => $e->getMessage(),
      ]);
    }

    return $result;
  }

  public function listBatches() {
    $result = [];

    try {
      $result = $this->batchApi->listBatches();
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('Smartling failed to list batches:<br/>Error: @error', [
        '@error' => $e->getMessage(),
      ]);
    }

    return $result;
  }

  public function getBatchStatus($batch_uid) {
    $result = [];

    try {
      $result = $this->batchApi->getBatchStatus($batch_uid);
    }
    catch (SmartlingApiException $e) {
      $this->logger->error('Smartling failed to get batch @batch_uid status:<br/>Error: @error', [
        '@batch_uid' => $batch_uid,
        '@error' => $e->getMessage(),
      ]);
    }

    return $result;
  }

}
