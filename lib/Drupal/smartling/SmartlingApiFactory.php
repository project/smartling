<?php

/**
 * @file
 * SmartlingApiFactory.php.
 */

namespace Drupal\smartling;

use Drupal;
use Drupal\smartling\Settings\SmartlingSettingsHandler;
use Exception;
use Smartling\AuthApi\AuthTokenProvider;
use Smartling\Batch\BatchApi;
use Smartling\Context\ContextApi;
use Smartling\File\FileApi;
use Smartling\Jobs\JobsApi;
use Smartling\Project\ProjectApi;

/**
 * Class SmartlingApiFactory
 * @package Drupal\smartling
 */
class SmartlingApiFactory {

  /**
   * Returns API object as a service.
   *
   * @param \Drupal\smartling\Settings\SmartlingSettingsHandler $settings
   * @param string $api_type
   * @return \Smartling\BaseApiAbstract
   * @throws \Exception
   * @throws \Smartling\Exceptions\SmartlingApiException
   */
  public static function create(SmartlingSettingsHandler $settings, $api_type = 'file') {
    require_once __DIR__ . '/../../../vendor/autoload.php';

    ConnectorInfo::setUpCurrentClientInfo();

    $auth_provider = AuthTokenProvider::create($settings->getUserId(), $settings->getTokenSecret());
    $logger = smartling_log_get_handler();
    $api = NULL;

    switch ($api_type) {
      case 'file':
        $api = FileApi::create($auth_provider, $settings->getProjectId(), $logger);

        break;

      case 'project':
        $api = ProjectApi::create($auth_provider, $settings->getProjectId(), $logger);

        break;

       case 'jobs':
        $api = JobsApi::create($auth_provider, $settings->getProjectId(), $logger);

        break;

      case 'batch':
        $api = BatchApi::create($auth_provider, $settings->getProjectId(), $logger);

        break;

      case 'context':
        $api = ContextApi::create($auth_provider, $settings->getProjectId(), $logger);

        break;

      default:
        throw new Exception('Unsupported API has been requested: ' . $api_type);
    }

    return $api;
  }

}
