<?php
namespace Drupal\smartling\EntityConversionUtils;

class EntityConversionUtil implements EntityConversionInterface {
  protected $settings;
  protected $entityApiWrapper;
  protected $fieldApiWrapper;
  protected $drupalApiWrapper;
  protected $smartlingUtils;

  public function __construct($settings, $entity_api_wrapper, $field_api_wrapper, $drupal_api_wrapper, $smartling_utils) {
    $this->settings = $settings;
    $this->fieldApiWrapper = $field_api_wrapper;
    $this->drupalApiWrapper = $drupal_api_wrapper;
    $this->smartlingUtils = $smartling_utils;
    $this->entityApiWrapper = $entity_api_wrapper;
  }

  public function convert(&$entity, $entity_type) {
    $default_language = $this->drupalApiWrapper->getDefaultLanguage();
    if ($default_language == LANGUAGE_NONE) {
      return FALSE;
    }
    $bundle = $this->entityApiWrapper->getBundle($entity_type, $entity);
    $allowed_fields = $this->settings->getFieldsSettingsByBundle($entity_type, $bundle);

    if (empty($allowed_fields)) {
      return FALSE;
    }

    $this->updateToFieldsTranslateMethod($entity, $entity_type, $default_language, $allowed_fields);

    //some magic transformations so that "title" module could catch up the title.
    $this->entityApiWrapper->entitySave($entity_type, $entity);
    $id = $this->entityApiWrapper->getID($entity_type, $entity);
    $entity = $this->entityApiWrapper->entityLoadSingle($entity_type, $id);
  }

  public function updateToFieldsTranslateMethod($entity, $entity_type, $default_language, $allowed_fields) {
    $field_langs = $this->fieldApiWrapper->fieldLanguage($entity_type, $entity);

    foreach ($field_langs as $field => $lang) { // go through ALL field of this node
      if (($lang == LANGUAGE_NONE) && (in_array($field, $allowed_fields))) { // if the field is in the wrong language
        $items = $this->fieldApiWrapper->fieldGetItems($entity_type, $entity, $field, $lang); // get all field values
        if (!empty($items)) {
          $entity->{$field}[$default_language] = $items; // put it under language neutral
          unset($entity->{$field}[$lang]); // remove the old language
        }
      }
    }
    $entity->language = $default_language;// set the node language to neutral
  }


}
