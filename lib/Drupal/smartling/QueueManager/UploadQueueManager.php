<?php

/**
 * @file
 * Contains Drupal\smartling\Forms.
 */

namespace Drupal\smartling\QueueManager;

class UploadQueueManager implements QueueManagerInterface {

  protected $smartlingUtils;
  protected $entityProcessorFactory;
  protected $smartlingSubmissionWrapper;
  protected $drupalWrapper;
  protected $settings;
  protected $fileTransport;
  protected $apiWrapper;

  public function __construct(
    $smartling_submission_wrapper,
    $entity_processor_factory,
    $settings,
    $smartling_utils,
    $drupal_wrapper,
    $file_transport,
    $api_wrapper
  ) {
    $this->smartlingSubmissionWrapper = $smartling_submission_wrapper;
    $this->entityProcessorFactory = $entity_processor_factory;
    $this->smartlingUtils = $smartling_utils;
    $this->drupalWrapper = $drupal_wrapper;
    $this->settings = $settings;
    $this->fileTransport = $file_transport;
    $this->apiWrapper = $api_wrapper;
  }

  /**
   * @inheritdoc
   */
  public function add($data) {
    if (empty($data)) {
      return FALSE;
    }
    $smartling_queue = \DrupalQueue::get('smartling_upload');
    $smartling_queue->createQueue();
    return $smartling_queue->createItem($data);
  }

  /**
   * @inheritdoc
   */
  public function execute($data) {
    $result = TRUE;

    if (!$this->smartlingUtils->isConfigured()) {
      throw new \Drupal\smartling\SmartlingExceptions\SmartlingNotConfigured(t('Smartling module is not configured. Please follow the page <a href="@link">"Smartling settings"</a> to setup Smartling configuration.', array('@link' => url('admin/config/regional/smartling'))));
    }

    // Fallback behavior for previous implementation when $data was an array of
    // eids. Now data is array with 'eids', 'batch_uid', 'execute_batch' etc
    // properties.
    if (isset($data['eids'])) {
      if (!is_array($data['eids'])) {
        $data['eids'] = [$data['eids']];
      }
      $eids = array_unique($data['eids']);
    }
    else {
      if (!is_array($data)) {
        $data = [$data];
      }
      $eids = array_unique($data);
    }

    $target_locales = [];
    $entity_data_array = [];

    foreach ($eids as $eid) {
      $this->smartlingSubmissionWrapper->loadByID($eid);
      if ($this->smartlingSubmissionWrapper->isEmpty()) {
        continue;
      }
      $file_name = $this->smartlingSubmissionWrapper->getFileName();
      $target_locales[$file_name][] = $this->smartlingSubmissionWrapper->getTargetLanguage();
      $entity_data_array[$file_name][] = clone $this->smartlingSubmissionWrapper;
    }


    foreach ($entity_data_array as $file_name => $entity_array) {
      $submission = reset($entity_array);
      $processor = $this->entityProcessorFactory->getProcessor($submission->getEntity());
      $content = $processor->exportContent();

      // Fallback behavior: if $batch_uid == NULL then upload file through
      // File API. If isset - upload through Batch API.
      $batch_uid = isset($data['batch_uid']) ? $data['batch_uid'] : NULL;
      $event = $this->fileTransport->upload($content, $submission, $target_locales[$file_name], $batch_uid);

      if ($event == SMARTLING_STATUS_EVENT_FAILED_UPLOAD) {
        $result = FALSE;
      }

      foreach ($entity_array as $submission) {
        $submission->setStatusByEvent($event)->save();

        $this->drupalWrapper->rulesInvokeEvent('smartling_after_submission_upload_event', [$submission->getEID()]);
      }
    }

    if (!empty($data['execute_batch'])) {
      $this->apiWrapper->executeBatch($data['batch_uid']);
    }

    return $result;
  }
}
