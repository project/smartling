<?php

/**
 * @file
 * Contains Drupal\smartling\Forms.
 */

namespace Drupal\smartling\QueueManager;

use Drupal\smartling\ApiWrapperInterface;
use Drupal\smartling\Log\LoggerInterface;
use Drupal\smartling\Wrappers\SmartlingEntityDataWrapper;
use Drupal\smartling\Wrappers\SmartlingEntityDataWrapperCollection;
use Drupal\smartling\Wrappers\SmartlingUtils;

class CheckStatusQueueManager implements QueueManagerInterface {

  /**
   * @var ApiWrapperInterface
   */
  protected $apiWrapper;
  /** @var  SmartlingEntityDataWrapper */
  protected $smartlingSubmissionWrapper;
  /** @var  DownloadQueueManager */
  protected $queueDownload;
  /** @var  LoggerInterface */
  protected $log;
  /** @var  SmartlingUtils */
  protected $smartlingUtils;
  /** @var  SmartlingEntityDataWrapperCollection */
  protected $submissionsCollection;

  protected $drupalWrapper;

  public function __construct(
    $api_wrapper,
    $smartling_submission_wrapper,
    $submissions_collection,
    $queue_download,
    $log,
    $smartling_utils,
    $drupal_wrapper
  ) {
    $this->apiWrapper = $api_wrapper;
    $this->smartlingSubmissionWrapper = $smartling_submission_wrapper;
    $this->submissionsCollection = $submissions_collection;
    $this->queueDownload = $queue_download;
    $this->log = $log;
    $this->smartlingUtils = $smartling_utils;
    $this->drupalWrapper = $drupal_wrapper;
  }

  /**
   * @inheritdoc
   */
  public function add($eids) {
    //$smartling_entities = smartling_entity_data_load_multiple($eids);
    $smartling_entities = $this->submissionsCollection->loadByIDs($eids)
      ->getCollection();

    $smartling_queue = \DrupalQueue::get('smartling_check_status');
    $smartling_queue->createQueue();
    foreach ($smartling_entities as $queue_item) {
      $eid = $queue_item->getEID();
      $file_name = $queue_item->getFileName();
      if (!empty($file_name)) {
        $smartling_queue->createItem($eid);
        $this->log->info('Add item to "smartling_check_status" queue. Smartling entity data id - @eid, related entity id - @rid, entity type - @entity_type',
          array(
            '@eid' => $queue_item->getEID(),
            '@rid' => $queue_item->getRID(),
            '@entity_type' => $queue_item->getEntityType()
          ));
      }
      elseif ($queue_item->status != 0) {
        $this->log->warning('Original file name is empty. Smartling entity data id - @eid, related entity id - @rid, entity type - @entity_type',
          array(
            '@eid' => $queue_item->getEID(),
            '@rid' => $queue_item->getRID(),
            '@entity_type' => $queue_item->getEntityType()
          ));
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function execute($eids) {
    if (!$this->smartlingUtils->isConfigured()) {
      throw new \Drupal\smartling\SmartlingExceptions\SmartlingNotConfigured(t('The Smartling module is not configured. Please go to <a href="@link">Smartling settings</a> to finish configuration.', array('@link' => url('admin/config/regional/smartling'))));
    }

    if (!is_array($eids)) {
      $eids = array($eids);
    }

    $result = array();
    foreach ($eids as $eid) {
      $smartling_submission = $this->smartlingSubmissionWrapper->loadByID($eid)
        ->getEntity();

      if (empty($smartling_submission)) {
        continue;
      }
      $request_result = $this->apiWrapper->getStatus($smartling_submission);

      $statuses = $this->apiWrapper->getStatusAllLocales($smartling_submission->file_name);
      $last_modified = $this->apiWrapper->getLastModified($smartling_submission->file_name);

      if (empty($statuses) || empty($last_modified)) {
        $this->log->warning('Something went wrong for file with name: "@file_name". Submission: @submission, statuses: @statuses, last modified date: @last_modified', array(
          '@file_name' => $smartling_submission->file_name,
          '@submission' => print_r($smartling_submission, TRUE),
          '@statuses' => print_r($statuses, TRUE),
          '@last_modified' => print_r($last_modified, TRUE)
        ));

        return $result;
      }

      $result[$eid] = $request_result;

      $target_language = $smartling_submission->target_language;
      $authorized = intval($statuses[$target_language]['authorizedStringCount']);
      $completed = intval($statuses[$target_language]['completedStringCount']);
      $progress = ($authorized + $completed > 0) ? (int) (($completed / ($authorized + $completed)) * 100) : 0;

      if (($progress === 100) && ($last_modified[$target_language] > $smartling_submission->last_modified)
        && ($smartling_submission->entity_type != 'smartling_interface_entity')
      ) {
        $this->queueDownload->add($smartling_submission->eid);
      }

      $smartling_submission->progress = $progress;
      $this->smartlingSubmissionWrapper->setEntity($smartling_submission)->save();
      $this->drupalWrapper->rulesInvokeEvent('smartling_after_submission_check_status_event', array($eid));
    }

    return $result;
  }
}
