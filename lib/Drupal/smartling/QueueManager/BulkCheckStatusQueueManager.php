<?php

/**
 * @file
 * Contains Drupal\smartling\Forms.
 */

namespace Drupal\smartling\QueueManager;

use Drupal\smartling\ApiWrapperInterface;
use Drupal\smartling\Log\LoggerInterface;
use Drupal\smartling\Wrappers\SmartlingEntityDataWrapper;
use Drupal\smartling\Wrappers\SmartlingEntityDataWrapperCollection;
use Drupal\smartling\Wrappers\SmartlingUtils;
use Smartling\Exceptions\SmartlingApiException;

class BulkCheckStatusQueueManager {//implements QueueManagerInterface {

  /**
   * @var ApiWrapperInterface
   */
  protected $apiWrapper;
  /** @var  SmartlingEntityDataWrapper */
  protected $smartlingSubmissionWrapper;
  /** @var  DownloadQueueManager */
  protected $queueDownload;
  /** @var  LoggerInterface */
  protected $log;
  /** @var  SmartlingUtils */
  protected $smartlingUtils;
  /** @var  SmartlingEntityDataWrapperCollection */
  protected $submissionsCollection;

  protected $drupalWrapper;

  public function __construct(
    $api_wrapper,
    $smartling_submission_wrapper,
    $submissions_collection,
    $queue_download,
    $log,
    $smartling_utils,
    $drupal_wrapper
  ) {
    $this->apiWrapper = $api_wrapper;
    $this->smartlingSubmissionWrapper = $smartling_submission_wrapper;
    $this->submissionsCollection = $submissions_collection;
    $this->queueDownload = $queue_download;
    $this->log = $log;
    $this->smartlingUtils = $smartling_utils;
    $this->drupalWrapper = $drupal_wrapper;
  }

  /**
   * @inheritdoc
   */
  public function add($file_name) {
    if (!is_string($file_name)) {
      return;
    }

    $smartling_queue = \DrupalQueue::get('smartling_bulk_check_status');
    $smartling_queue->createQueue();
    $smartling_queue->createItem($file_name);
    $this->log->info('Add item to "smartling_bulk_check_status" queue. File name: @file_name',
      array(
        '@file_name' => $file_name,
      ));
  }

  /**
   * @inheritdoc
   */
  public function execute($file_name) {
    if (!$this->smartlingUtils->isConfigured()) {
      throw new \Drupal\smartling\SmartlingExceptions\SmartlingNotConfigured(t('The Smartling module is not configured. Please go to <a href="@link">Smartling settings</a> to finish configuration.', array('@link' => url('admin/config/regional/smartling'))));
    }

    if (!is_string($file_name)) {
      return;
    }

    $submissions = $this->submissionsCollection->loadByCondition(array('file_name' => $file_name))->getCollection();

    try {
      $statuses = $this->apiWrapper->getStatusAllLocales($file_name);
      $last_modified = $this->apiWrapper->getLastModified($file_name);
    }
    catch (SmartlingApiException $e) {
      if (!empty($submissions)) {
        foreach ($submissions as $submission) {
          $submission->status = SMARTLING_STATUS_PENDING_CANCEL;
          smartling_entity_data_save($submission);

          $this->log->warning(
            'The file @filename could not be found in Smartling. The submission @submission_id is rejected. Please resubmit it in order to upload file in Smartling.', [
              '@filename' => $file_name,
              '@submission_id' => $submission->eid,
            ]
          );
        }

        return;
      }
    }

    if (empty($submissions) || empty($statuses) || empty($last_modified)) {
      $this->log->warning('Something went wrong for file with name: "@file_name". Submissions: @submissions, statuses: @statuses, last modified date: @last_modified',
        array(
          '@file_name' => $file_name,
          '@submissions' => print_r($submissions, TRUE),
          '@statuses' => print_r($statuses, TRUE),
          '@last_modified' => print_r($last_modified, TRUE)
        ));
      return;
    }

    $result = array();
    foreach ($submissions as $submission) {

      if (empty($submission)) {
        continue;
      }

      $target_language = $submission->target_language;

      if (!isset($statuses[$target_language]) || !isset($last_modified[$target_language])) {
        $submission->status = SMARTLING_STATUS_PENDING_CANCEL;
        smartling_entity_data_save($submission);

        $this->log->warning(
          'The locale @locale could not be found in Smartling. The submission @submission_id is rejected.', [
            '@locale' => smartling_convert_locale_drupal_to_smartling($target_language),
            '@submission_id' => $submission->eid,
          ]
        );

        continue;
      }

      $authorized = intval($statuses[$target_language]['authorizedStringCount']);
      $completed = intval($statuses[$target_language]['completedStringCount']);
      $progress = ($authorized + $completed > 0) ? (int) (($completed / ($authorized + $completed)) * 100) : 0;

      if (($progress === 100) && ($last_modified[$target_language] > $submission->last_modified)
        && ($submission->entity_type != 'smartling_interface_entity')
      ) {
        $this->queueDownload->add($submission->eid);
      }


      $submission->progress = $progress;

      if ($last_modified[$target_language] > $submission->last_modified) {
        $submission->last_modified = $last_modified[$target_language];
      }

      //smartling_entity_data_save($result['entity_data']);
      $this->smartlingSubmissionWrapper->setEntity($submission)
        ->save();

      $this->drupalWrapper->rulesInvokeEvent('smartling_after_submission_check_status_event', array($submission->eid));

    }

    return $result;
  }
}
