<?php

/**
 * @file
 * Contains Drupal\smartling\Forms.
 */

namespace Drupal\smartling\QueueManager;

use Drupal\smartling\EntityConversionUtils\EntityConversionFactory;
use Drupal\smartling\Log\LoggerInterface;
use Drupal\smartling\Settings\SmartlingSettingsHandler;
use Drupal\smartling\Wrappers\SmartlingEntityDataWrapperCollection;
use Drupal\smartling\Wrappers\SmartlingUtils;

class UploadRouter {
  /**
   * @var SmartlingEntityDataWrapperCollection
   */
  protected $entityWrapperCollection;
  /**
   * @var UploadQueueManager
   */
  protected $queueManager;
  /**
   * @var LoggerInterface
   */
  protected $log;
  /**
   * @var SmartlingSettingsHandler
   */
  protected $settings;
  /**
   * @var EntityConversionFactory
   */
  protected $entityConversionFactory;
  /**
   * @var SmartlingUtils
   */
  protected $smartlingUtils;

  public function __construct($entity_wrapper_collection, $upload_manager, $log, $settings, $entity_conversion_factory, $smartling_utils) {
    $this->entityWrapperCollection = $entity_wrapper_collection;
    $this->queueManager = $upload_manager;
    $this->log = $log;
    $this->settings = $settings;
    $this->entityConversionFactory = $entity_conversion_factory;
    $this->smartlingUtils = $smartling_utils;
  }

  public function routeUploadRequest(
    $entity_type,
    $entity,
    $languages,
    $job_id = NULL,
    $batch_uid = NULL,
    $async_mode = NULL,
    $execute_batch = TRUE
  ) {
    $default_language = smartling_get_default_language();

    if ($this->settings->getConvertEntitiesBeforeTranslation()) {
      $this->entityConversionFactory->getConverter($entity_type)
        ->convert($entity, $entity_type);
    }

    if (entity_language($entity_type, $entity) !== $default_language) {
      $this->log->info(
        'Entity is not in the site default language and cannot be submitted for translation. Type - @entity_type, Entity - @entity',
        [
          '@entity_type' => $entity_type,
          '@entity' => print_r($entity, TRUE)
        ]
      );

      return [
        'status' => 0,
        'message' => 'Entity in non-default language',
        'type' => 'info',
      ];
    }

    $async_mode = (is_null($async_mode)) ? $this->settings->getAsyncMode() : $async_mode;

    $languages = array_filter($languages);
    $success = $this->entityWrapperCollection->createForLanguages($entity_type, $entity, $languages, $job_id);
    if (!$success) {
      return ['status' => 0, 'message' => '', 'type' => 'warning'];
    }

    $type = 'status';
    $langs = implode(', ', $languages);

    if ($async_mode) {
      $this->queueManager->add([
        'eids' => $this->entityWrapperCollection->getIDs(),
        'job_id' => $job_id,
        'batch_uid' => $batch_uid,
        'execute_batch' => $execute_batch,
      ]);

      $collection = $this->entityWrapperCollection->getCollection();
      $smartling_wrapper = reset($collection);

      // Create content hash (Fake entity update).
      $this->smartlingUtils->hookEntityUpdate($entity, $entity_type);

      $this->log->info('Smartling queue task was created for entity id - @id, locale - @locale, type - @entity_type',
        array(
          '@id' => $smartling_wrapper->getRID(),
          '@locale' => $langs,
          '@entity_type' => $entity_type
        ));

      $user_message = t('The @entity_type "@title" has been queued for translation in Smartling to "@langs". File will be uploaded to Smartling by next cron runs.', [
        '@entity_type' => $entity_type,
        '@title' => $smartling_wrapper->getTitle(),
        '@langs' => $langs,
      ]);
    }
    else {
      $result = $this->queueManager->execute([
        'eids' => $this->entityWrapperCollection->getIDs(),
        'job_id' => $job_id,
        'batch_uid' => $batch_uid,
        'execute_batch' => $execute_batch,
      ]);

      $collection = $this->entityWrapperCollection->getCollection();
      $smartling_wrapper = reset($collection);
      $title = $smartling_wrapper->getTitle();

      $user_message = t('The @entity_type "@title" has been sent to Smartling for translation to "@langs".', [
        '@entity_type' => $entity_type,
        '@title' => $title,
        '@langs' => $langs,
      ]);

      if (!$result) {
        $type = 'error';
        $user_message = t('The @entity_type "@title" hasn\'t been sent to Smartling for translation to "@langs".', [
          '@entity_type' => $entity_type,
          '@title' => $title,
          '@langs' => $langs,
        ]);
      }
    }
    return array('status' => 1, 'type' => $type, 'message' => $user_message);
  }

}
