<?php

/**
 * @file
 * Contains Drupal\smartling\Forms.
 */

namespace Drupal\smartling\QueueManager;

class DownloadQueueManager implements QueueManagerInterface {

  protected $smartlingSubmissionWrapper;
  protected $fieldApiWrapper;
  protected $entityProcessorFactory;
  protected $smartlingUtils;
  protected $drupalWrapper;
  protected $fileTransport;


  public function __construct(
    $smartling_submission_wrapper,
    $field_api_wrapper,
    $entity_processor_factory,
    $smartling_utils,
    $drupal_wrapper,
    $file_transport
  ) {
    $this->smartlingSubmissionWrapper = $smartling_submission_wrapper;
    $this->fieldApiWrapper = $field_api_wrapper;
    $this->entityProcessorFactory = $entity_processor_factory;
    $this->smartlingUtils = $smartling_utils;
    $this->drupalWrapper = $drupal_wrapper;
    $this->fileTransport = $file_transport;
  }

  /**
   * @inheritdoc
   */
  public function add($eids) {
    if (empty($eids)) {
      return;
    }
    $smartling_queue = \DrupalQueue::get('smartling_download');
    $smartling_queue->createQueue();
    $smartling_queue->createItem($eids);
  }

  /**
   * @inheritdoc
   */
  public function execute($eids) {
    if (!drupal_is_cli()) {
      if ($this->drupalWrapper->getDefaultLanguage() != $this->fieldApiWrapper->fieldValidLanguage(NULL, FALSE)
      ) {
        throw new \Drupal\smartling\SmartlingExceptions\WrongSiteSettingsException('The download failed. Please switch to the Smartling\'s default language: ' . $this->drupalWrapper->getDefaultLanguage());
      }
    }

    if (!$this->smartlingUtils->isConfigured()) {
      throw new \Drupal\smartling\SmartlingExceptions\SmartlingNotConfigured(t('Smartling module is not configured. Please follow the page <a href="@link">"Smartling settings"</a> to setup Smartling configuration.', array('@link' => url('admin/config/regional/smartling'))));
    }

    if (!is_array($eids)) {
      $eids = array($eids);
    }

    $global_status = TRUE;
    foreach ($eids as $eid) {
      $status = FALSE;

      $smartling_submission_wrapper = $this->smartlingSubmissionWrapper->loadByID($eid);
      $smartling_submission = $smartling_submission_wrapper->getEntity();
      if ($smartling_submission) {
        $downloaded_content = $this->fileTransport->download($smartling_submission_wrapper);
        if ($downloaded_content) {
          $processor = $this->entityProcessorFactory->getProcessor($smartling_submission);
          $status = $processor->updateEntity($downloaded_content);
        }
        $this->drupalWrapper->rulesInvokeEvent('smartling_after_submission_download_event', array($eid));
      }
      $global_status = $global_status & $status;
    }

    return $global_status;
  }
}
