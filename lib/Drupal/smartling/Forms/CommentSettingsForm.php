<?php

namespace Drupal\smartling\Forms;

class CommentSettingsForm extends GenericEntitySettingsForm {

  public function __construct($settings, $logger, $api_wrapper) {
    parent::__construct($settings, $logger, $api_wrapper);

    $this->entityNameTranslated = t('Comment');
    $this->entityKey = '#entity';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smartling_get_comment_settings_form';
  }
}
