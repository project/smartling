<?php

namespace Drupal\smartling\Forms;

class BaseBulkTranslateJobForm extends GenericEntitySettingsForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form['smartling'] = [
      '#title' => t('Smartling management'),
      '#type' => 'container',
      '#attributes' => ['id' => ['smartling_fieldset']],
      '#attached' => [
        'css' => [
          drupal_get_path('module', 'smartling') . '/css/smartling_entity_settings.css' => [
            'type' => 'file',
          ],
          drupal_get_path('module', 'smartling') . '/css/smartling_admin.css' => [
            'type' => 'file',
          ],
        ],
        'js' => [
          drupal_get_path('module', 'smartling') . '/js/smartling_check_all.js',
          drupal_get_path('module', 'smartling') . '/js/smartling_jobs_time.js',
          drupal_get_path('module', 'smartling') . '/js/moment/moment.min.js',
          drupal_get_path('module', 'smartling') . '/js/moment/moment-timezone-with-data.min.js',
          [
            'data' => [
              'smartling' => [
                'checkAllId' => [
                  'edit-create-new-job-tab-target',
                  'edit-add-to-existing-job-tab-container-job-info-target',
                ],
              ],
            ],
            'type' => 'setting',
          ],
        ],
      ],
      '#modal' => TRUE,
      'smartling_users_time_zone' => [
        '#type' => 'hidden',
      ],
    ];

    $form['smartling']['content'] = [
      '#type' => 'container',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function submitCreateJob(array &$form, array &$form_state) {
    if (!smartling_is_configured()) {
      return [
        'batch_uid' => NULL,
        'job_id' => NULL,
        'target_locales' => [],
      ];
    }

    $job_result = parent::submitCreateJob($form, $form_state);
    $batch_result = $this->submitCreateBatch($job_result['job_id'], $job_result['authorize']);

    return [
      'batch_uid' => $batch_result['batch_uid'],
      'job_id' => $job_result['job_id'],
      'target_locales' => $job_result['target_locales'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function submitAddToJob(array &$form, array &$form_state) {
    if (!smartling_is_configured()) {
      return [
        'batch_uid' => NULL,
        'job_id' => NULL,
        'target_locales' => [],
      ];
    }

    $job_result = parent::submitAddToJob($form, $form_state);
    $batch_result = $this->submitCreateBatch($job_result['job_id'], $job_result['authorize']);

    return [
      'batch_uid' => $batch_result['batch_uid'],
      'job_id' => $job_result['job_id'],
      'target_locales' => $job_result['target_locales'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function targetLangFormElem($id, $entity_type, $entity, $default_language) {
    return [
      '#type' => 'checkboxes',
      '#title' => 'Target Locales',
      '#options' => smartling_language_options_list(),
    ];
  }

}
