<?php

namespace Drupal\smartling\Forms;

use Smartling\Jobs\JobStatus;

class BulkTranslateAddToJobForm extends BaseBulkTranslateJobForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form = parent::buildForm($form, $form_state);
    $target = $this->targetLangFormElem(NULL, NULL, NULL, NULL);

    unset($form['smartling']['content']);

    $form['smartling']['add_to_existing_job_tab'] = [
        '#type' => 'fieldset',
        '#title' => t('Add to Existing Job'),
        '#group' => 'content',
        '#tree' => TRUE,
      ] + $this->addToJobFormPart([
        JobStatus::AWAITING_AUTHORIZATION,
        JobStatus::IN_PROGRESS,
        JobStatus::COMPLETED,
      ], $target, !empty($form_state['values']['add_to_existing_job_tab']) ? $form_state['values']['add_to_existing_job_tab'] : [], FALSE);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    return $this->submitAddToJob($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    $this->validateAddToJob($form, $form_state);
  }

}
