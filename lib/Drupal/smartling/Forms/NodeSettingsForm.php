<?php

namespace Drupal\smartling\Forms;

use Drupal\smartling\ApiWrapperInterface;
use Drupal\smartling\Log\LoggerInterface;
use Drupal\smartling\Settings\SmartlingSettingsHandler;

class NodeSettingsForm extends GenericEntitySettingsForm {

  public function __construct(SmartlingSettingsHandler $settings, LoggerInterface $logger, ApiWrapperInterface $api_wrapper) {
    parent::__construct($settings, $logger, $api_wrapper);

    $this->entityNameTranslated = t('Node');
    $this->entityKey = '#entity';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smartling_get_node_settings_form';
  }

  public function getOriginalEntityWrapper($entity_type, $entity) {
    $node = smartling_get_original_node($entity);

    return parent::getOriginalEntityWrapper($entity_type, $node);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form = parent::buildForm($form, $form_state);
    $entity = $form[$this->entityKey];

    // On a node edit form we have a "Provide a menu link" checkbox that
    // creates a link to node in selected menu. Drupal core has a bug
    // (including 7.45) https://www.drupal.org/node/1247506 that removes node
    // from menu while node_save() process (more details by link). Despite the
    // fact that it isn't Smartling but Drupal core bug we fix it in this way:
    // set $entity->menu['enabled'] to "TRUE" only if menu item does exist and
    // this option enabled for current node. Otherwise hook menu_node_save()
    // will delete menu item.
    if (!empty($entity->menu['mlid']) && !isset($entity->menu['enabled'])) {
      $entity->menu['enabled'] = TRUE;
    }

    return $form;
  }

  protected function targetLangFormElem($id, $entity_type, $entity, $default_language) {
    // This is node for fields method translate or original for nodes method.
    if (($entity->tnid == '0') || ($entity->tnid == $entity->nid)) {
      $languages = smartling_language_list();
    }
    elseif ($entity->tnid != $entity->nid) {
      // This is node for nodes method translate | not original.
      $languages = smartling_language_list();
      $node_original = node_load($entity->tnid);
      unset($languages[$node_original->language]);
    }

    if (!is_null($id)) {
      $check = array();

      if (($entity->tnid != '0') && ($entity->tnid != $entity->nid)) {
        // For not original node in nodes translate method.
        $translations = translation_node_get_translations($entity->tnid);
        $original_nid = FALSE;
        // Get original.
        foreach ($translations as $langcode => $value) {
          if ($translations[$langcode]->nid == $entity->tnid) {
            $original_nid = $translations[$langcode]->nid;
            break;
          }
        }

        foreach ($languages as $d_locale => $language) {
          //if ($language->enabled != '0') {

          $entity_data = smartling_entity_load_by_conditions(array(
            'rid' => $original_nid,
            'entity_type' => $entity_type,
            'target_language' => $d_locale,
          ));
          $language_name = check_plain($language->name);

          if ($entity_data !== FALSE) {
            $options[$d_locale] = smartling_entity_status_message(t('Node'), $entity_data->status, $language_name, $entity_data->progress);
          }
          else {
            $options[$d_locale] = $language_name;
          }

          $check[] = (($entity_data) && (!in_array($entity_data->status, array(SMARTLING_STATUS_PENDING_CANCEL, SMARTLING_STATUS_CANCELED)))) ? $d_locale : FALSE;
          //}
        }
      }
      elseif (($entity->tnid != '0') && ($entity->tnid == $entity->nid)) {
        // For original node in nodes translate method.
        $translations = translation_node_get_translations($entity->tnid);
        $original_nid = FALSE;
        // Get original.
        foreach ($translations as $langcode => $value) {
          if ($translations[$langcode]->nid == $entity->tnid) {
            $original_nid = $translations[$langcode]->nid;
            break;
          }
        }

        foreach ($languages as $d_locale => $language) {

          if ($default_language != $d_locale) {//&& $language->enabled != '0') {

            $entity_data = smartling_entity_load_by_conditions(array(
              'rid' => $original_nid,
              'entity_type' => $entity_type,
              'target_language' => $d_locale,
            ));
            $language_name = check_plain($language->name);

            if ($entity_data !== FALSE) {
              $options[$d_locale] = smartling_entity_status_message(
                t('Node'),
                $entity_data->status,
                $language_name,
                $entity_data->progress
              );
            }
            else {
              $options[$d_locale] = $language_name;
            }

            $check[] = (($entity_data) && (!in_array($entity_data->status, array(SMARTLING_STATUS_PENDING_CANCEL, SMARTLING_STATUS_CANCELED)))) ? $d_locale : FALSE;
          }
        }
      }
      else {
        // For fieds method.
        foreach ($languages as $d_locale => $language) {
          if ($default_language != $d_locale) {//&& $language->enabled != '0') {

            $entity_data = smartling_entity_load_by_conditions(array(
              'rid' => $id,
              'entity_type' => $entity_type,
              'target_language' => $d_locale,
            ));
            $language_name = check_plain($language->name);

            if ($entity_data !== FALSE) {
              $options[$d_locale] = smartling_entity_status_message(
                t('Node'),
                $entity_data->status,
                $language_name,
                $entity_data->progress
              );
            }
            else {
              $options[$d_locale] = $language_name;
            }
            $check[] = (($entity_data) && (!in_array($entity_data->status, array(SMARTLING_STATUS_PENDING_CANCEL, SMARTLING_STATUS_CANCELED)))) ? $d_locale : FALSE;
          }
        }
      }

      $elem = array(
        '#type' => 'checkboxes',
        '#title' => 'Target Locales',
        '#options' => $options,
        '#default_value' => $check,
      );
    }
    else {
      foreach ($languages as $langcode => $language) {
        $options[$langcode] = check_plain($language->name);
      }

      $elem = array(
        '#type' => 'checkboxes',
        '#title' => 'Target Locales',
        '#options' => $options,
      );
    }
    return $elem;
  }
}
