<?php

namespace Drupal\smartling\Forms;

use Drupal\smartling\ApiWrapperInterface;
use Drupal\smartling\Log\LoggerInterface;
use Drupal\smartling\Settings\SmartlingSettingsHandler;

class TaxonomyTermSettingsForm extends GenericEntitySettingsForm {
  public function __construct(SmartlingSettingsHandler $settings, LoggerInterface $logger, ApiWrapperInterface $api_wrapper) {
    parent::__construct($settings, $logger, $api_wrapper);

    $this->entityNameTranslated = t('Term');
    $this->entityKey = '#term';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['smartling']['#weight'] = 100;

    return $form;
  }

  protected function getOriginalEntity($entity) {
    return smartling_get_original_taxonomy_term($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smartling_get_term_settings_form';
  }
}
