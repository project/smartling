<?php

namespace Drupal\smartling\Forms;

use DateTimeZone;
use Drupal\smartling\ApiWrapperInterface;
use Drupal\smartling\Log\LoggerInterface;
use Drupal\smartling\Settings\SmartlingSettingsHandler;
use Exception;
use Smartling\Jobs\JobStatus;

class GenericEntitySettingsForm implements FormInterface {

  protected $entityNameTranslated;
  protected $entityKey;
  protected $languageKey;
  protected $settings;
  protected $logger;
  protected $apiWrapper;

  public function __construct(SmartlingSettingsHandler $settings, LoggerInterface $logger, ApiWrapperInterface $api_wrapper) {
    $this->entityNameTranslated = t('Entity');
    $this->entityKey = '#entity';
    $this->languageKey = 'language';
    $this->settings = $settings;
    $this->logger = $logger;
    $this->apiWrapper = $api_wrapper;
  }

  protected function getOriginalEntity($entity) {
    return $entity;
  }

  protected function getOriginalEntityWrapper($entity_type, $entity) {
    return entity_metadata_wrapper($entity_type, $entity);
  }

  protected function targetLangFormElem($id, $entity_type, $entity, $default_language) {
    $languages = smartling_language_list();

    if (!is_null($id)) {
      foreach ($languages as $d_locale => $language) {
        //if ($language->enabled != '0') {

        $entity_data = smartling_entity_load_by_conditions(array(
          'rid' => $id,
          'entity_type' => $entity_type,
          'target_language' => $d_locale,
        ));
        $language_name = check_plain($language->name);

        if ($entity_data !== FALSE) {
          $options[$d_locale] = smartling_entity_status_message(
            $this->entityNameTranslated,
            $entity_data->status,
            $language_name,
            $entity_data->progress
          );
        }
        else {
          $options[$d_locale] = $language_name;
        }

        $check[] = (($entity_data) && (!in_array($entity_data->status, array(
            SMARTLING_STATUS_PENDING_CANCEL,
            SMARTLING_STATUS_CANCELED
          )))) ? $d_locale : FALSE;
        //}
      }

      $elem = array(
        '#type' => 'checkboxes',
        '#title' => 'Target Locales',
        '#options' => $options,
        '#default_value' => $check,
      );
    }
    else {
      foreach ($languages as $d_locale => $language) {
        $options[$d_locale] = check_plain($language->name);
      }

      $elem = array(
        '#type' => 'checkboxes',
        '#title' => 'Target Locales',
        '#options' => $options,
      );
    }
    return $elem;
  }

  /**
   * Returns "Create New Job" form part.
   *
   * @param array $target
   * @param bool $submit
   * @return array
   */
  protected function createJobFormPart(array $target, $submit = TRUE) {
    $form_part = [
      'name' => [
        '#type' => 'textfield',
        '#title' => t('Name !star', [
          '!star' => '<span class="form-required">*</span>',
        ]),
      ],
      'description' => [
        '#type' => 'textarea',
        '#title' => t('Description'),
      ],
      'due_date' => [
        '#type' => 'date_popup',
        '#date_year_range' => '-0:+5',
        '#date_format' => 'm/d/Y - H:i',
        '#default_value' => date('Y-m-d H:i', time()),
        '#title' => t('Due date'),
      ],
      'authorize' => [
        '#type' => 'checkbox',
        '#title' => t('Authorize'),
        '#default_value' => $this->settings->getAutoAuthorizeContent(),
      ],
      'target' => $target,
    ];

    if ($submit) {
      $form_part['submit'] = [
        '#type' => 'submit',
        '#value' => t('Create job'),
        '#submit' => [$this->getFormId() . '_submit'],
        '#validate' => [$this->getFormId() . '_validate'],
        '#states' => [
          'invisible' => [
            // Hide the button if term is language neutral.
            'select[name=language]' => ['value' => LANGUAGE_NONE],
          ],
        ],
      ];
    }

    return $form_part;
  }

  /**
   * Returns "Add to existing job" form part.
   *
   * @param array $statuses
   * @param array $target
   * @param array $add_to_job_values
   *
   * @param bool $submit
   * @return array
   * @throws \Exception
   */
  protected function addToJobFormPart(array $statuses, array $target, array $add_to_job_values, $submit = TRUE) {
    $available_jobs = $this->apiWrapper->listJobs(NULL, $statuses);

    if (empty($available_jobs['items'])) {
      $form = [
        'message' => [
          '#markup' => t('There are no available jobs'),
        ],
      ];
    }
    else {
      $options = [];
      $files = [];
      $project_id = $this->settings->getProjectId();

      foreach ($available_jobs['items'] as $item) {
        $options[$item['translationJobUid']] = $item['jobName'];
      }

      // Default values by first page load.
      if (empty($add_to_job_values)) {
        $selected_job_id = $available_jobs['items'][0]['translationJobUid'];
      }
      else {
        // Get default values from selected job.
        $selected_job_id = $add_to_job_values['container']['job_id'];
      }

      $selected_job = $this->apiWrapper->getJob($selected_job_id);
      $default_description = $selected_job['description'];
      $default_job_state = ucwords(strtolower(str_replace('_', ' ', $selected_job['jobStatus'])));
      $source_files = $selected_job['sourceFiles'];

      foreach ($source_files as $source_file) {
        $file_name = urlencode($source_file['name']);
        $files[] = l($source_file['name'], "https://dashboard.smartling.com/projects/{$project_id}/files/files.htm", [
          'fragment' => "file/{$file_name}",
          'attributes' => [
            'target' => '_blank',
          ],
        ]);
      }

      $form = [
        'container' => [
          '#prefix' => '<div id="smartling-job-form-wrapper">',
          '#suffix' => '</div>',
          '#type' => 'container',
          'job_id' => [
            '#type' => 'select',
            '#title' => t('Job'),
            '#options' => $options,
            '#ajax' => [
              'callback' => 'smartling_get_entity_settings_form_select_job_ajax_callback',
              'wrapper' => 'smartling-job-form-wrapper',
            ],
          ],
          'job_info' => [
            '#type' => 'fieldset',
            '#title' => t('Info'),
            '#collapsible' => TRUE,
            'dashboard_link' => [
              '#type' => 'item',
              '#markup' => l($selected_job['jobName'], "https://dashboard.smartling.com/app/projects/{$project_id}/jobs/{$selected_job_id}", [
                'attributes' => [
                  'target' => '_blank',
                ],
              ]),
              '#title' => t('Dashboard link'),
            ],
            'state' => [
              '#type' => 'item',
              '#markup' => $default_job_state,
              '#title' => t('State'),
            ],
            'description' => [
              '#type' => 'textarea',
              '#title' => t('Description'),
              '#value' => $default_description,
            ],
            'due_date' => [
              '#type' => 'date_popup',
              '#date_year_range' => '-0:+5',
              '#date_format' => 'm/d/Y - H:i',
              '#title' => t('Due date'),
              '#value' => [
                'date' => '',
                'time' => '',
              ],
            ],
            'utc_due_date_hidden' => [
              '#type' => 'hidden',
              '#value' => $selected_job['dueDate'],
            ],
            'authorize' => [
              '#type' => 'checkbox',
              '#title' => t('Authorize'),
              '#value' => $this->settings->getAutoAuthorizeContent(),
            ],
            'target' => $target,
            'name' => [
              '#type' => 'value',
              '#value' => $selected_job['jobName'],
            ],
            'files_container' => [
              '#type' => 'fieldset',
              '#collapsible' => TRUE,
              '#collapsed' => TRUE,
              '#title' => t('Files'),
              'files' => [
                '#markup' => !empty($files) ? theme('item_list', [
                  'items' => $files,
                  'type' => 'ol',
                ]) : t('There are no files inside this job'),
              ],
            ],
          ],
        ],
      ];
    }

    if ($submit && !empty($available_jobs['items'])) {
      $form['container']['job_info']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Add to job'),
        '#submit' => [$this->getFormId() . '_submit'],
        '#validate' => [$this->getFormId() . '_validate'],
        '#states' => [
          'invisible' => [
            // Hide the button if term is language neutral.
            'select[name=language]' => ['value' => LANGUAGE_NONE],
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * Ajax callback.
   *
   * @param $form
   * @param $form_state
   *
   * @return array
   */
  public function selectJobAjaxCallback($form, &$form_state) {
    return $form['smartling']['add_to_existing_job_tab']['container'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smartling_get_entity_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
//    if (isset($form_state['confirm_delete']) && $form_state['confirm_delete'] === TRUE) {
//      return array();
//    }

    $entity = $this->getOriginalEntity((object) $form[$this->entityKey]);
    $entity_type = $form['#entity_type'];
    if (empty($entity_type)) {
      $entity_type = $form_state['complete form']['#entity_type'];
    }
    $wrapper = entity_metadata_wrapper($entity_type, $entity);
    $bundle = $wrapper->getBundle();
    $id = $wrapper->getIdentifier();

    if (!smartling_translate_fields_configured($bundle, $entity_type)) {
      return array();
    }

    $form['smartling'] = [
      '#title' => t('Smartling management'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      //'#weight' => 100,
      '#group' => 'additional_settings',
      '#attributes' => ['id' => ['smartling_fieldset']],
      '#attached' => [
        'css' => [
          drupal_get_path('module', 'smartling') . '/css/smartling_entity_settings.css' => [
            'type' => 'file',
          ],
          drupal_get_path('module', 'smartling') . '/css/smartling_admin.css' => [
            'type' => 'file',
          ],
        ],
        'js' => [
          drupal_get_path('module', 'smartling') . '/js/smartling_check_all.js',
          drupal_get_path('module', 'smartling') . '/js/smartling_jobs_time.js',
          drupal_get_path('module', 'smartling') . '/js/moment/moment.min.js',
          drupal_get_path('module', 'smartling') . '/js/moment/moment-timezone-with-data.min.js',
          [
            'data' => [
              'smartling' => [
                'checkAllId' => [
                  'edit-target',
                  'edit-create-new-job-tab-target',
                  'edit-add-to-existing-job-tab-container-job-info-target',
                  'edit-locked-fields',
                ],
              ],
            ],
            'type' => 'setting'
          ],
        ],
      ],
      '#modal' => TRUE,
      'smartling_users_time_zone' => [
        '#type' => 'hidden',
      ],
    ];

    $default_language = smartling_get_default_language();
    $locale = field_valid_language(NULL, FALSE);

    if ($default_language !== $locale) {
      //Otherwise if "title" module is enabled - it will spoil the title of the original node.
      $form['smartling']['error_default_language'] = array(
        '#type' => 'markup',
        '#markup' => '<p>Please switch to Smartling\'s default language in order to execute upload/download operations. The default language is: ' . $default_language . '</p>',
      );

      $options = [];
      $wrapper = $this->getOriginalEntityWrapper($entity_type, $entity);
      $fields_enabled_for_translation = $this->settings->getFieldsSettingsByBundle($entity_type, $bundle);
      $entity_data = smartling_entity_load_by_conditions([
        'rid' => $wrapper->getIdentifier(),
        'entity_type' => $wrapper->type(),
        'target_language' => $locale,
      ]);

      // Don't show lock fields form if there is no submission yet for this
      // translation (manually added translation).
      if (!$entity_data) {
        return $form;
      }

      $default_values = drupal_container()
        ->get('smartling.wrappers.smartling_submission_wrapper')
        ->setEntity($entity_data)
        ->getLockedFields();

      foreach ($fields_enabled_for_translation as $field_name) {
        try {
          $options[$field_name] = $wrapper->getPropertyInfo($field_name)['label'];
        }
        catch (Exception $e) {
          $mapping = [
            'title_property_field' => t('Title'),
            'title_property' => t('Title'),
            'name_property_field' => t('Name'),
            'description_property_field' => t('Description'),
          ];

          $options[$field_name] = $mapping[$field_name];
        }
      }

      $form['smartling']['wrapper'] = [
        '#type' => 'fieldset',
        '#title' => t('Lock fields'),
      ];

      $form['smartling']['wrapper']['locked_fields'] = [
        '#type' => 'checkboxes',
        '#description' => t('Selected fields will not be overwritten.'),
        '#default_value' => $default_values,
        '#options' => $options,
      ];

      $form['smartling']['wrapper']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Save locked fields'),
        '#submit' => ['smartling_locked_fields_form_submit'],
      ];

      return $form;
    }

    if (entity_language($entity_type, $entity) !== $default_language) {
      $form['smartling']['error_default_language'] = array(
        '#type' => 'markup',
        '#markup' => '<p>Only content in the Smartling default language can be submitted to Smartling for translation.</p>',
      );
      return $form;
    }

    // Jobs tabs.
    $target = $this->targetLangFormElem($id, $entity_type, $entity, $form[$this->languageKey]['#default_value']);

    // We have to make some fields "required" and validate them manually because
    // we can't use '#required' property as it will validate all form submits
    // on entity edit pages.
    $target['#title'] = t('Target Locales !star', [
      '!star' => '<span class="form-required">*</span>',
    ]);

    $form['smartling']['content'] = [
      '#type' => 'vertical_tabs',
    ];

    // "Create new job" tab.
    $form['smartling']['create_new_job_tab'] = [
        '#type' => 'fieldset',
        '#title' => t('Create New Job'),
        '#group' => 'content',
        '#tree' => TRUE,
      ] + $this->createJobFormPart($target);

    // "Add to existing job" tab.
    $form['smartling']['add_to_existing_job_tab'] = [
      '#type' => 'fieldset',
      '#title' => t('Add to Existing Job'),
      '#group' => 'content',
      '#tree' => TRUE,
    ] + $this->addToJobFormPart([
      JobStatus::AWAITING_AUTHORIZATION,
      JobStatus::IN_PROGRESS,
      JobStatus::COMPLETED,
    ], $target, !empty($form_state['values']['add_to_existing_job_tab']) ? $form_state['values']['add_to_existing_job_tab'] : []);

    $form['smartling']['download_translation'] = [
      '#type' => 'fieldset',
      '#group' => 'content',
      '#title' => t('Download translation'),
    ];

    $form['smartling']['download_translation']['target'] = $target;

    $form['smartling']['download_translation']['submit_to_download'] = [
      '#type' => 'submit',
      '#value' => 'Download Translation',
      '#submit' => ['smartling_download_translate_form_submit'],
      '#states' => [
        'invisible' => [
          // Hide the button if term is language neutral.
          'select[name=language]' => ['value' => LANGUAGE_NONE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Validates "Create job" form part.
   *
   * @param array $form
   * @param array $form_state
   */
  protected function validateCreateJob(array &$form, array &$form_state) {
    if (empty($form_state['values']['create_new_job_tab']['name'])) {
      form_error($form['smartling']['create_new_job_tab']['name'], t('@name field is required.', [
        '@name' => t('Job Name'),
      ]));
    }
    else {
      $response = $this->apiWrapper->listJobs($form_state['values']['create_new_job_tab']['name']);

      if (!empty($response['items'])) {
        foreach ($response['items'] as $item) {
          if ($item['jobName'] == $form_state['values']['create_new_job_tab']['name']) {
            form_error(
              $form['smartling']['create_new_job_tab']['name'],
              t('Job with name "@name" already exists. Please choose another job name.', [
                '@name' => $form_state['values']['create_new_job_tab']['name'],
              ])
            );

            break;
          }
        }
      }
    }

    if (empty(array_filter($form_state['values']['create_new_job_tab']['target']))) {
      form_error($form['smartling']['create_new_job_tab']['target'], t('@name field is required.', [
        '@name' => t('Job Target Locales'),
      ]));
    }

    if (!empty($form_state['values']['create_new_job_tab']['due_date'])) {
      $date_string = $form_state['input']['create_new_job_tab']['due_date']['date'] . ' ' . $form_state['input']['create_new_job_tab']['due_date']['time'];

      if (!$this->validateDateString($date_string)) {
        form_error(
          $form['smartling']['create_new_job_tab']['due_date'],
          t('Due date value is invalid.')
        );

        return;
      }

      $due_date = strtotime($form_state['values']['create_new_job_tab']['due_date']);

      if ($due_date < time()) {
        form_error($form['smartling']['create_new_job_tab']['due_date'], t('Due date can not be in the past.'));
      }
    }
  }

  /**
   * Validates "Add to job" form part.
   *
   * @param array $form
   * @param array $form_state
   */
  protected function validateAddToJob(array &$form, array &$form_state) {
    if (empty(array_filter($form_state['input']['add_to_existing_job_tab']['container']['job_info']['target']))) {
      form_error($form['smartling']['add_to_existing_job_tab']['container']['job_info']['target'], t('@name field is required.', [
        '@name' => t('Job Target Locales'),
      ]));
    }

    if (!empty($form_state['input']['add_to_existing_job_tab']['container']['job_info']['due_date']['date']) &&
      !empty($form_state['input']['add_to_existing_job_tab']['container']['job_info']['due_date']['time'])
    ) {
      $date_string = $form_state['input']['add_to_existing_job_tab']['container']['job_info']['due_date']['date'] . ' ' . $form_state['input']['add_to_existing_job_tab']['container']['job_info']['due_date']['time'];

      if (!$this->validateDateString($date_string)) {
        form_error(
          $form['smartling']['add_to_existing_job_tab']['container']['job_info']['due_date'],
          t('Due date value is invalid.')
        );

        return;
      }

      $due_date = strtotime($date_string);

      if ($due_date < time()) {
        form_error(
          $form['smartling']['add_to_existing_job_tab']['container']['job_info']['due_date'],
          t('Due date can not be in the past.')
        );
      }
    }
  }

  /**
   * @param $date_string
   *
   * @return \DateTime
   */
  protected function validateDateString($date_string) {
    return \DateTime::createFromFormat('m/d/Y H:i', $date_string);
  }

  /**
   * Submits "Create job" form part.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return array
   */
  protected function submitCreateJob(array &$form, array &$form_state) {
    $job_attributes = $form_state['values']['create_new_job_tab'];

    if (!empty($job_attributes['due_date'])) {
      $users_time_zone = new DateTimeZone($form_state['values']['smartling_users_time_zone']);
      $job_attributes['due_date'] = new \DateTime($job_attributes['due_date'], $users_time_zone);
      $job_attributes['due_date']->setTimeZone(new DateTimeZone('UTC'));
    }

    $job_result = $this->apiWrapper->createJob(
      $job_attributes['name'],
      $job_attributes['description'],
      $job_attributes['due_date'],
      $job_attributes['target']
    );

    if (empty($job_result['job_id'])) {
      foreach ($job_result['errors'] as $error) {
        drupal_set_message($error, 'error');
      }

      drupal_set_message(t('Job has not been created. See <a href="@url">logs</a> for more information.', ['@url' => url('/admin/reports/dblog')]), 'error');

      return [
        'job_id' => NULL,
        'authorize' => FALSE,
        'target_locales' => [],
      ];
    }

    return [
      'job_id' => $job_result['job_id'],
      'authorize' => $job_attributes['authorize'],
      'target_locales' => $job_attributes['target'],
    ];
  }

  /**
   * Submits "Add to job" form part.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return array
   */
  protected function submitAddToJob(array &$form, array &$form_state) {
    $job_attributes = $form_state['values']['add_to_existing_job_tab']['container'];
    $job_raw_attributes = $form_state['input']['add_to_existing_job_tab']['container'];
    $job_id = $job_raw_attributes['job_id'];
    $job_authorize = empty($job_raw_attributes['job_info']['authorize']) ? FALSE : (bool) $job_raw_attributes['job_info']['authorize'];
    $drupal_target_locales = $job_raw_attributes['job_info']['target'];
    $due_date = NULL;

    if (!empty($job_raw_attributes['job_info']['due_date']['date']) &&
      !empty($job_raw_attributes['job_info']['due_date']['time'])
    ) {
      $date = $job_raw_attributes['job_info']['due_date']['date'] . ' ' . $job_raw_attributes['job_info']['due_date']['time'];
      $users_time_zone = new DateTimeZone($form_state['values']['smartling_users_time_zone']);
      $due_date = new \DateTime($date, $users_time_zone);
      $due_date->setTimeZone(new DateTimeZone('UTC'));
    }

    // TODO 7.x-4.x: detect changes.
    //  $old_description = $job_attributes['job_info']['description'];
    //  $new_description = $job_raw_attributes['job_info']['description'];
    //  $old_due_date_timestamp = strtotime($job_attributes['job_info']['due_date']);
    //  $new_due_date_timestamp = strtotime($job_raw_attributes['job_info']['due_date']['date'] . ' ' . $job_raw_attributes['job_info']['due_date']['time']);

    $job_result = $this->apiWrapper->updateJob(
      $job_id,
      $job_attributes['job_info']['name'],
      $job_raw_attributes['job_info']['description'],
      $due_date
    );

    if (empty($job_result['job_id'])) {
      foreach ($job_result['errors'] as $error) {
        drupal_set_message($error, 'error');
      }

      drupal_set_message(t('Job has not been updated. See <a href="@url">logs</a> for more information.', ['@url' => url('/admin/reports/dblog')]), 'error');

      return [
        'job_id' => NULL,
        'authorize' => FALSE,
        'target_locales' => [],
      ];
    }

    return [
      'job_id' => $job_result['job_id'],
      'authorize' => $job_authorize,
      'target_locales' => $drupal_target_locales,
    ];
  }

  /**
   * Creates Smartling batch.
   *
   * @param $job_id
   * @param $job_authorize
   *
   * @return array
   */
  protected function submitCreateBatch($job_id, $job_authorize) {
    $batch_result = $this->apiWrapper->createBatch($job_id, $job_authorize);

    if (empty($batch_result['batch_uid'])) {
      foreach ($batch_result['errors'] as $error) {
        drupal_set_message($error, 'error');
      }

      drupal_set_message(t('Smartling Batch operation has not been created. See <a href="@url">logs</a> for more information.', ['@url' => url('/admin/reports/dblog')]), 'error');

      return [
        'batch_uid' => NULL,
      ];
    }

    return $batch_result;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    // Take into consideration ajax requests where triggering element can look
    // like "edit-create-new-job-tab-submit--N".
    if (strpos($form_state['triggering_element']['#id'], 'edit-create-new-job-tab') !== FALSE) {
      $this->validateCreateJob($form, $form_state);
    }

    if (strpos($form_state['triggering_element']['#id'], 'edit-add-to-existing-job-tab') !== FALSE) {
      $this->validateAddToJob($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    if (!smartling_is_configured()) {
      return;
    }

    $job_id = NULL;
    $job_authorize = FALSE;
    $drupal_target_locales = [];

    // Get job id: create job or select existing one.
    // Take into consideration ajax requests where triggering element can look
    // like "edit-create-new-job-tab-submit--N".
    if (strpos($form_state['triggering_element']['#id'], 'edit-create-new-job-tab') !== FALSE) {
      $job_result = $this->submitCreateJob($form, $form_state);

      $job_id = $job_result['job_id'];
      $job_authorize = $job_result['authorize'];
      $drupal_target_locales = $job_result['target_locales'];
    }

    if (strpos($form_state['triggering_element']['#id'], 'edit-add-to-existing-job-tab') !== FALSE) {
      $job_result = $this->submitAddToJob($form, $form_state);

      $job_id = $job_result['job_id'];
      $job_authorize = $job_result['authorize'];
      $drupal_target_locales = $job_result['target_locales'];
    }

    if (empty($job_id)) {
      return;
    }

    $batch_result = $this->submitCreateBatch($job_id, $job_authorize);

    if (empty($batch_result['batch_uid'])) {
      return;
    }

    $res = drupal_container()
      ->get('smartling.queue_managers.upload_router')
      ->routeUploadRequest($form['#entity_type'], $form[$this->entityKey], $drupal_target_locales, $job_id, $batch_result['batch_uid']);

    if (!empty($res['status'])) {
      drupal_set_message($res['message'], $res['type']);
    }
  }

}
