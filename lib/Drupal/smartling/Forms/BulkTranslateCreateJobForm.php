<?php

namespace Drupal\smartling\Forms;

class BulkTranslateCreateJobForm extends BaseBulkTranslateJobForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form = parent::buildForm($form, $form_state);

    unset($form['smartling']['content']);

    $form['smartling']['create_new_job_tab'] = [
      '#type' => 'fieldset',
      '#title' => t('Create New Job'),
      '#tree' => TRUE,
    ] + $this->createJobFormPart($this->targetLangFormElem(NULL, NULL, NULL, NULL), FALSE);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    return $this->submitCreateJob($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    $this->validateCreateJob($form, $form_state);
  }

}
