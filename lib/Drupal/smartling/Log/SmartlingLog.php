<?php

/**
 * @file
 * Smartling log.
 */

namespace Drupal\smartling\Log;

use Drupal\smartling\ConnectorInfo;
use Drupal\smartling\SmartlingApiFactory;
use GuzzleHttp\Client;
use Exception;
use Smartling\BaseApiAbstract;

/**
 * Class SmartlingLog.
 */
class SmartlingLog extends DevNullLogger {

  public $settings;

  private $timeOut;
  private $buffer;
  private $bufferLimit;
  private $host;
  private $logLevel;
  private $uid;

  /**
   * Map of PSR3 log constants to RFC 5424 log constants.
   *
   * @var array
   */
  private $levelTranslation = [
    RfcLogLevel::EMERGENCY => LogLevel::EMERGENCY,
    RfcLogLevel::ALERT => LogLevel::ALERT,
    RfcLogLevel::CRITICAL => LogLevel::CRITICAL,
    RfcLogLevel::ERROR => LogLevel::ERROR,
    RfcLogLevel::WARNING => LogLevel::WARNING,
    RfcLogLevel::NOTICE => LogLevel::NOTICE,
    RfcLogLevel::INFO => LogLevel::INFO,
    RfcLogLevel::DEBUG => LogLevel::DEBUG,
  ];

  public function __construct(
    $settings,
    $log_level = RfcLogLevel::DEBUG,
    $host = 'https://api.smartling.com/updates/status',
    $buffer_limit = 100,
    $time_out = 5
  ) {
    $this->settings = $settings;
    $this->buffer = [];
    $this->logLevel = $log_level;
    $this->host = $host;
    $this->bufferLimit = $buffer_limit;
    $this->timeOut = $time_out;
    $this->uid = uniqid();

    drupal_register_shutdown_function([$this, 'flush']);
  }

  public function log($level, $message, array $context = array(), $ignore_settings = FALSE) {
    if (!$ignore_settings && !$this->settings->getLogMode()) {
      return FALSE;
    }

    $link = '';
    if (isset($context['entity_link'])) {
      $link = $context['entity_link'];
      unset($context['entity_link']);
    }

    // Pass "variables" parameter as NULL since we don't need to translate logs.
    watchdog('smartling', format_string($message, $context), NULL, $level, $link);

    if ($level <= $this->logLevel && $this->settings->getCloudLogMode()) {
      $this->buffer[] = [
        'level' => $level,
        'message' => $message,
        'context' => $context,
        'datetime' => date('Y-m-d H:i:s', time()),
      ];

      // Flush buffer on overflow.
      if ((count($this->buffer) % $this->bufferLimit) === 0) {
        $this->flush();
      }
    }

    return TRUE;
  }

  /**
   * Log messages into needed destination.
   */
  public function flush() {
    if (empty($this->buffer)) {
      return;
    }

    try {
      $records = [];
      $project_id = $this->settings->getProjectId();
      $host = php_uname('n');
      $http_host = $_SERVER['HTTP_HOST'];

      // Assemble records.
      foreach ($this->buffer as $drupal_log_record) {
        $records[] = [
          'level_name' => $this->levelTranslation[$drupal_log_record['level']],
          'channel' => 'drupal-classic-connector',
          'datetime' => $drupal_log_record['datetime'],
          'context' => [
            'projectId' => $project_id,
            'host' => $host,
            'http_host' => $http_host,
            'moduleVersion' => ConnectorInfo::getLibVersion(),
            'requestId' => $this->uid,
          ],
          'message' => format_string($drupal_log_record['message'], $drupal_log_record['context']),
        ];
      }

      $client = new Client();
      $client->request('POST', $this->host, [
        'json' => [
          'records' => $records,
        ],
        'timeout' => $this->timeOut,
        'headers' => [
          'User-Agent' => ConnectorInfo::getLibName() . '/' . ConnectorInfo::getLibVersion(),
        ],
      ]);
    }
    catch (Exception $e) {
    }

    $this->buffer = [];
  }

}
