<?php

/**
 * @file
 * Smartling log.
 */

namespace Drupal\smartling\Utils;

use Drupal\smartling\ApiWrapperInterface;
use Drupal\smartling\Log\LoggerInterface;
use Drupal\smartling\QueueManager\UploadQueueManager;
use Drupal\smartling\Settings\SmartlingSettingsHandler;
use Drupal\smartling\Wrappers\DrupalAPIWrapper;
use Smartling\Jobs\JobStatus;

/**
 * Class BucketJobManager.
 */
class BucketJobManager {

  private $apiWrapper;
  private $drupalApiWrapper;
  private $uploadManager;
  private $logger;
  private $settings;

  public function __construct(
    LoggerInterface $logger,
    ApiWrapperInterface $api_wrapper,
    DrupalAPIWrapper $drupal_api_wrapper,
    UploadQueueManager $upload_manager,
    SmartlingSettingsHandler $settings
  ) {
    $this->logger = $logger;
    $this->apiWrapper = $api_wrapper;
    $this->drupalApiWrapper = $drupal_api_wrapper;
    $this->uploadManager = $upload_manager;
    $this->settings = $settings;
  }

  private function getName($suffix = '') {
    $date = date('m/d/Y');
    $name = "Daily Bucket Job $date";
    $this->drupalApiWrapper->alter('smartling_bucket_job_name', $name);

    return $name . $suffix;
  }

  public function handle(array $smartling_entities) {
    $job_uid = NULL;
    $eids = [];
    $job_name = $this->getName();
    $response = $this->apiWrapper->listJobs($job_name, [
      JobStatus::AWAITING_AUTHORIZATION,
      JobStatus::IN_PROGRESS,
      JobStatus::COMPLETED,
    ]);

    // Try to find the latest created bucket job.
    if (!empty($response['items'])) {
      $job_uid = $response['items'][0]['translationJobUid'];
    }

    // If there is no existing bucket job then create new one.
    if (empty($job_uid)) {
      $job_result = $this->apiWrapper->createJob($job_name, t('Bucket job: contains updated content.'), NULL, []);

      // If there is a CANCELED/CLOSED bucket job then we have to come with new
      // job name in order to avoid "Job name is already taken" error.
      if (empty($job_result['job_id'])) {
        $job_name = $this->getName(' ' . date('H:i:s'));
        $job_result = $this->apiWrapper->createJob($job_name, t('Bucket job: contains updated content.'), NULL, []);
      }

      $job_uid = $job_result['job_id'];
    }

    if (empty($job_uid)) {
      $this->logger->error("Queueing file upload into the bucket job failed: can't find/create job.");

      return;
    }

    $batch_result = $this->apiWrapper->createBatch($job_uid, $this->settings->getAutoAuthorizeContent());

    if (empty($batch_result['batch_uid'])) {
      $this->logger->error("Queueing file upload into the bucket job failed: can't create batch.");

      return;
    }

    foreach ($smartling_entities as $item) {
      $eids[] = $item->eid;
    }

    $this->uploadManager->add([
      'eids' => $eids,
      'job_id' => $job_uid,
      'batch_uid' => $batch_result['batch_uid'],
      'execute_batch' => TRUE,
    ]);
  }

}
