<?php

/**
 * @file
 * Smartling log.
 */

namespace Drupal\smartling\Utils;

/**
 * Class FileNameManager.
 */
class FileNameManager {
  const FILE_NAME_MAX_LENGTH = 255;

  protected $fileTypeMap;

  public function __construct($file_type_map, $drupal_api_wrapper) {
    $this->fileTypeMap = $file_type_map;
    $drupal_api_wrapper->alter('smartling_file_type_map', $this->fileTypeMap);
  }

  public function buildFileName($submission) {
    $entity_type = $submission->getEntityType();

    if (isset($this->fileTypeMap[$entity_type]) && $this->fileTypeMap[$entity_type] == 'gettext') {
      $file_name = 'smartling_interface_translation_' . $submission->getBundle() . '.pot';
    }
    else {
      $file_name = $this->assembleAndShortenFileNameIfNeeded(
        $this->getSanitizedTitle($submission),
        $this->getSuffix($submission),
        ".xml"
      );
    }

    return $file_name;
  }


  public function buildTranslatedFileName($submission) {
    $file_name = $submission->getFileName();
    $file_name = substr($file_name, 0, strlen($file_name) - 4);

    if ($submission->getEntityType() == 'smartling_interface_entity') {
      $file_name = $file_name . '_' . $submission->getTargetLanguage() . '.po';
    }
    else {
      $file_name = $this->assembleAndShortenFileNameIfNeeded(
        $this->getSanitizedTitle($submission),
        $this->getSuffix($submission) . "_" . $submission->getTargetLanguage(),
        ".xml"
      );
    }

    return $file_name;
  }

  private function getSanitizedTitle($submission) {
    return strtolower(trim(preg_replace('#\W+#', '_', $submission->getTitle()), '_'));
  }

  private function getSuffix($submission) {
    return '_' . $submission->getEntityType() . '_' . $submission->getRID();
  }

  private function assembleAndShortenFileNameIfNeeded($sanitizedTitle, $suffix, $extension) {
    $file_name = $sanitizedTitle . $suffix . $extension;
    $file_name_length = strlen($file_name);

    if ($file_name_length > FileNameManager::FILE_NAME_MAX_LENGTH) {
      $diff = $file_name_length - FileNameManager::FILE_NAME_MAX_LENGTH;
      $file_name = substr($sanitizedTitle, 0, strlen($sanitizedTitle) - $diff) . $suffix . $extension;
    }

    return $file_name;
  }

}
