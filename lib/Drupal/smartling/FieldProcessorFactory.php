<?php

/**
 * @file
 * Contains Drupal\smartling\FieldProcessor\FieldProcessorFactory.
 */

namespace Drupal\smartling;

/**
 * Factory that creates field processor instances and contains mapping.
 *
 * @package Drupal\smartling\FieldProcessors
 */
class FieldProcessorFactory {

  protected $fieldMapping;
  protected $log;
  protected $fieldApiWrapper;
  protected $entityApiWrapper;
  protected $drupalApiWrapper;
  protected $smartlingUtils;

  /**
   * @param array $field_mapping
   * @param SmartlingLog $logger
   * @param $field_api_wrapper
   * @param $entity_api_wrapper
   * @param $drupal_api_wrapper
   * @param $smartling_utils
   */
  public function __construct($field_mapping, $logger, $field_api_wrapper, $entity_api_wrapper, $drupal_api_wrapper, $smartling_utils) {
    $this->log = $logger;
    $this->fieldApiWrapper = $field_api_wrapper;
    $this->entityApiWrapper = $entity_api_wrapper;
    $this->drupalApiWrapper = $drupal_api_wrapper;
    $this->smartlingUtils = $smartling_utils;

    $this->drupalApiWrapper->alter('smartling_field_processor_mapping_info', $field_mapping);
    $this->fieldMapping = $field_mapping;
  }

  public function getContainer() {
    return drupal_container();
  }

  /**
   * Factory method for FieldProcessor instances.
   *
   * @param string $field_name
   * @param \stdClass $entity
   * @param string $entity_type
   * @param \stdClass $smartling_submission
   *
   * @return BaseFieldProcessor
   */
  public function getProcessor($field_name, $entity, $entity_type, $smartling_submission, $target_language, $source_language = NULL) {
    $field_info = $this->fieldApiWrapper->fieldInfoField($field_name);

    if ($field_info) {
      $type = $field_info['type'];
      // @todo we could get notice about invalid key here.
      $class_name = $this->fieldMapping['real'][$type];
    }
    elseif (isset($this->fieldMapping['fake'][$field_name])) {
      $type = $field_name;
      $class_name = $this->fieldMapping['fake'][$type];
    }
    else {
      $this->log->warning("Smartling found unexisted field - @field_name", array('@field_name' => $field_name), TRUE);
      return FALSE;
    }

    if (!$class_name) {
      $this->log->warning("Smartling didn't process content of field - @field_name", array('@field_name' => $field_name), TRUE);
      return FALSE;
    }

    if (empty($source_language)) {
      $source_language = ($this->smartlingUtils->fieldIsTranslatable($field_name, $entity_type)) ? $this->entityApiWrapper->entityLanguage($entity_type, $entity) : LANGUAGE_NONE;
    }


    if ($type === 'field_collection') {
      $field_processor = new $class_name(
        $field_name,
        $entity,
        $entity_type,
        $smartling_submission,
        $source_language,
        $target_language,
        $this->drupalApiWrapper,
        $this,
        $this->entityApiWrapper,
        $this->fieldApiWrapper
      );
    }
    else {
      $field_processor = new $class_name(
        $field_name,
        $entity,
        $entity_type,
        $smartling_submission,
        $source_language,
        $target_language,
        $this->drupalApiWrapper
      );
    }

    return $field_processor;
    //return $container->get($service_id);
  }

  public function isSupportedField($field_name) {
    $supported = FALSE;
    $field_info = $this->fieldApiWrapper->fieldInfoField($field_name);

    if ($field_info) {
      $type = $field_info['type'];
      $supported = isset($this->fieldMapping['real'][$type]);
    }
    elseif (isset($this->fieldMapping['fake'][$field_name])) {
      $type = $field_name;
      $supported = isset($this->fieldMapping['fake'][$type]);
    }
    return (bool) $supported;
  }
}
