<?php

/**
 * @file
 * Contains Drupal\smartling\ApiWrapperInterface.
 */

namespace Drupal\smartling;

use DateTime;
use Smartling\File\FileApi;

interface ApiWrapperInterface {

  /**
   * Set Smartling API instance.
   *
   * @param $api
   *   Smartling API object.
   *
   * @param string $api_type
   *   Api type.
   *
   * @return
   */
  public function setApi($api, $api_type = 'file');

  /**
   * File API.
   *
   * Download file from service.
   *
   * @param object $entity
   *   Smartling transaction entity.
   *
   * @return \DOMDocument|boolean
   *   Return xml dom from downloaded file.
   */
  public function downloadFile($entity);

  /**
   * File API.
   *
   * Get status of given entity's translation progress.
   *
   * @param object $entity
   *   Smartling transaction entity.
   *
   * @return array|null
   *   Return status.
   */
  public function getStatus($entity);

  /**
   * Project API.
   *
   * Test Smartling API instance init and connection to Smartling server.
   *
   * @param array $locales
   *   List of locales in Drupal format.
   *
   * @return array
   *   If connections were successful for each locale.
   */
  public function testConnection(array $locales);

  /**
   * File API.
   *
   * Upload local file to Smartling for translation.
   *
   * @param string $file_path
   *   Real path to file.
   * @param string $file_name_unic
   *   Unified file name.
   * @param string $file_type
   *   File type. Use only 2 values 'xml' or 'getext'
   * @param array $locales
   *   List of locales in Drupal format.
   * @param int $batch_uid
   *   Batch identifier.
   *
   * @return string
   *   SMARTLING_STATUS_EVENT_UPLOAD_TO_SERVICE | SMARTLING_STATUS_EVENT_FAILED_UPLOAD
   */
  public function uploadFile($file_path, $file_name_unic, $file_type, array $locales, $batch_uid);

  // TODO: 3.x - never used?
  /**
   * get locale list for project
   *
   * @return array of locales
   */
  public function getLocaleList();

  /**
   * Context API.
   *
   * Uploads context file to Smartling and writes some logs.
   *
   * @param array $data
   * @return int
   */
  public function uploadContext($data);

  /**
   * File API.
   *
   * Returns status for all locales.
   *
   * @param $file_name
   * @return mixed
   */
  public function getStatusAllLocales($file_name);

  /**
   * File API.
   *
   * Returns file last modified date.
   * @param $file_name
   * @return mixed
   */
  public function getLastModified($file_name);

  /**
   * Jobs API.
   *
   * Creates job.
   *
   * @param $name
   * @param $description
   * @param $due_date
   * @param array $locales
   * @return mixed
   */
  public function createJob($name, $description, DateTime $due_date = NULL, array $locales = []);

  /**
   * Jobs API.
   *
   * Updates job.
   *
   * @param $job_id
   * @param $name
   * @param null $description
   * @param $due_date
   * @return mixed
   */
  public function updateJob($job_id, $name, $description = NULL, DateTime $due_date = NULL);

  /**
   * Jobs API.
   *
   * Cancels a job.
   *
   * @param $job_id
   * @return mixed
   */
  public function cancelJob($job_id);

  /**
   * Jobs API.
   *
   * Returns list of jobs.
   *
   * @param null $name
   * @param array $statuses
   * @return mixed
   */
  public function listJobs($name = NULL, array $statuses = []);

  /**
   * Jobs API.
   *
   * Returns job.
   *
   * @param $job_id
   * @return mixed
   */
  public function getJob($job_id);

  /**
   * Batch API.
   *
   * Creates batch.
   *
   * @param $job_id
   * @param $authorize
   * @return mixed
   */
  public function createBatch($job_id, $authorize);

  /**
   * Batch API.
   *
   * Executes batch.
   *
   * @param $batch_uid
   * @return mixed
   */
  public function executeBatch($batch_uid);

  /**
   * Batch API.
   *
   * List batches.
   *
   * @return mixed
   */
  public function listBatches();

  /**
   * Batch API.
   *
   * Get batch status.
   *
   * @param $batch_uid
   * @return mixed
   */
  public function getBatchStatus($batch_uid);

}
