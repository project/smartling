<?php

/**
 * @file
 * Contains Drupal\smartling\Processors\NodeProcessor.
 */

namespace Drupal\smartling\Processors;

class NodeProcessor extends GenericEntityProcessor {

  protected $fieldApiWrapper;

  public function __construct(
    $smartling_submission,
    $field_processor_factory,
    $smartling_settings,
    $log,
    $entity_api_wrapper,
    $smartling_utils,
    $field_api_wrapper
  ) {
    parent::__construct($smartling_submission, $field_processor_factory, $smartling_settings, $log, $entity_api_wrapper, $smartling_utils);

    $this->fieldApiWrapper = $field_api_wrapper;
  }

  /**
   * {inheritdoc}
   */
  protected function addTranslatedFieldsToNode($node, $original_node) {
    $field_values = array();
    foreach ($this->getTranslatableFields() as $field_name) {
      if (!empty($original_node->{$field_name}[LANGUAGE_NONE])) {
        $field_processor = $this->fieldProcessorFactory->getProcessor($field_name, $node, $this->drupalEntityType,
          $this->smartlingSubmission->getEntity(), $this->targetFieldLanguage);

        $val = $field_processor->cleanBeforeClone($node, $original_node);
        if (!empty($val)) {
          $field_values[$field_name] = $val;
        }
      }
    }

    $this->entityApiWrapper->nodeObjectPrepare($node);
    $this->entityApiWrapper->entitySave('node', $node);

    foreach ($this->getTranslatableFields() as $field_name) {
      if (!empty($field_values[$field_name])) {
        $node->{$field_name} = $field_values[$field_name];
      }
    }

    foreach ($this->getTranslatableFields() as $field_name) {
      // Run all translatable fields through prepareBeforeDownload
      // to make sure that all related logic was triggered.
      if (!empty($this->contentEntity->{$field_name}[LANGUAGE_NONE])) {
        $field_processor = $this->fieldProcessorFactory->getProcessor($field_name, $node, $this->drupalEntityType,
          $this->smartlingSubmission->getEntity(), $this->targetFieldLanguage);

        // @TODO get rid of hardcoded language.
        $field_processor->prepareBeforeDownload($this->contentEntity->{$field_name}[LANGUAGE_NONE]);
      }
    }

    return $node;
  }

  protected function prepareDrupalEntity() {
    if (!$this->isOriginalEntityPrepared && $this->smartlingUtils->isNodesMethod($this->smartlingSubmission->getBundle())) {
      $this->isOriginalEntityPrepared = TRUE;
      // Translate subnode instead of main one.
      $this->ifFieldMethod = FALSE;
      $tnid = $this->contentEntity->tnid ?: $this->contentEntity->nid;
      $translations = $this->entityApiWrapper->translationNodeGetTranslations($tnid);
      if (isset($translations[$this->drupalTargetLocale])) {
        $original_node = $translations[$this->drupalOriginalLocale];
        $original_node = $this->entityApiWrapper->entityLoadSingle('node', $original_node->nid);

        $this->smartlingSubmission->setRID($translations[$this->drupalTargetLocale]->nid);

        $node = $this->entityApiWrapper->entityLoadSingle(
          'node',
          $this->smartlingSubmission->getRID()
        ); //node_load($this->smartling_submission->rid);
        $node->translation_source = $this->contentEntity;

        //$node = node_load($node->nid);
        $node = $this->entityApiWrapper->entityLoadSingle('node', $node->nid);
        $node = $this->addTranslatedFieldsToNode($node, $original_node);

        $this->contentEntity = $node;
        $this->contentEntityWrapper->set($this->contentEntity);
      }
      else {
        // If node not exist, need clone.
        $node = clone $this->contentEntity;
        unset($node->nid);
        unset($node->vid);

        $node->status = $this->smartlingSettings->getPublishCompletedTranslation();

        // Reset alias settings of cloned node. Otherwise root's node alias will
        // be overridden.
        if (!empty($node->path['pathauto'])) {
          // Generate new alias for target language node.
          $node->path = [
            'pathauto' => 1,
          ];
        }
        else {
          $node->path = [
            'pathauto' => 0,
          ];
        }

        $this->entityApiWrapper->nodeObjectPrepare($node);
        $node->language = $this->drupalTargetLocale;
        $node->uid = $this->smartlingSubmission->getSubmitter();
        $node->tnid = $this->contentEntity->nid;

        // @todo Do we need this? clone should do all the stuff.
        $node_fields = $this->fieldApiWrapper->fieldInfoInstances('node', $this->contentEntity->type);
        foreach ($node_fields as $field) {
          $node->{$field['field_name']} = $this->contentEntity->{$field['field_name']};
        }

        $node->translation_source = $this->contentEntity;

        $node = $this->addTranslatedFieldsToNode($node, $this->contentEntity);
        $node = $this->entityApiWrapper->entityLoadSingle('node', $node->nid);
        // Second saving is done for Field Collection field support
        // that need host entity id.
        //node_save($node);

        // Update reference to drupal content entity.
        $this->contentEntity = $node;
        $this->smartlingSubmission->setRID($node->nid);
      }
    }
  }

  public static function supportedType($bundle) {
    $transl_method = variable_get('language_content_type_' . $bundle, NULL);
    return in_array($transl_method, array(
      SMARTLING_NODES_METHOD_KEY,
      SMARTLING_FIELDS_METHOD_KEY
    ));
  }

  protected function getOriginalEntity($entity) {
    return smartling_get_original_node($entity);
  }
}
