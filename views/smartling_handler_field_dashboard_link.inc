<?php

/**
 * @file
 * Handler code.
 */

/**
 * Handler for field dashboard_link.
 */
class SmartlingHandlerFieldDashboardLink extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function query() {}

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    try {
      $settings_handler = smartling_settings_get_handler();
      $project_id = $settings_handler->getProjectId();

      $result = l(t('Dashboard'), "https://dashboard.smartling.com/projects/{$project_id}/files/files.htm", array(
        'fragment' => "file/{$values->smartling_entity_data_file_name}",
        'attributes' => array(
          'target' => '_blank',
        ),
      ));
    } catch (\Exception $e) {
      watchdog('smartling', $e->getMessage() . '   ' . $e->getTraceAsString(), array(), WATCHDOG_CRITICAL);

      $result = t('Link is unavailable');
    }

    return $result;
  }

}
