<?php

/**
 * @file
 * Handler code.
 */

/**
 * Handler for field status.
 */
class SmartlingHandlerFieldEntityStatus extends views_handler_field {

  /**
   * Render.
   *
   * @param object $values
   *   Entity.
   *
   * @return string
   *   Return status value.
   */

  public function render($values) {
    $options = array(
      0 => 'New',
      1 => 'In Progress',
      2 => 'Completed',
      3 => 'Changed',
      4 => 'Failed',
      5 => 'Pending Cancel',
      6 => 'Canceled',
    );


    $status = intval($this->get_value($values));
    $status = isset($options[$status])?$options[$status]:'Unknown';

    return $status;
  }
}
