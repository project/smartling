<?php

namespace Drupal\smartling_translate_page_popup\Forms;

use Drupal\smartling\Forms\GenericEntitySettingsForm;
use Drupal\smartling\SmartlingExceptions\SmartlingGenericException;

class PageEntitiesTranslationForm extends GenericEntitySettingsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smartling_page_entities_translation_form';
  }

  public function buildForm(array $form, array &$form_state) {
    //Don't want to use DIC for the render of this form, as it is processed on every page for admin and
    //the issue is not solved yet: https://www.drupal.org/node/2495617
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreateJob(array &$form, array &$form_state) {
    if (empty($form_state['values']['create_new_job_tab']['name'])) {
      form_error($form['smartling']['job']['create_new_job_tab']['name'], t('@name field is required.', [
        '@name' => t('Job Name'),
      ]));
    }
    else {
      $response = $this->apiWrapper->listJobs($form_state['values']['create_new_job_tab']['name']);

      if (!empty($response['items'])) {
        foreach ($response['items'] as $item) {
          if ($item['jobName'] == $form_state['values']['create_new_job_tab']['name']) {
            form_error(
              $form['smartling']['job']['create_new_job_tab']['name'],
              t('Job with name "@name" already exists. Please choose another job name.', [
                '@name' => $form_state['values']['create_new_job_tab']['name'],
              ])
            );

            break;
          }
        }
      }
    }

    if (empty(array_filter($form_state['values']['create_new_job_tab']['target']))) {
      form_error($form['smartling']['job']['create_new_job_tab']['target'], t('@name field is required.', [
        '@name' => t('Job Target Locales'),
      ]));
    }

    if (!empty($form_state['values']['create_new_job_tab']['due_date'])) {
      $due_date = strtotime($form_state['values']['create_new_job_tab']['due_date']);

      if ($due_date < time()) {
        form_error($form['smartling']['job']['create_new_job_tab']['due_date'], t('Due date can not be in the past.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function validateAddToJob(array &$form, array &$form_state) {
    if (empty(array_filter($form_state['input']['add_to_existing_job_tab']['container']['job_info']['target']))) {
      form_error(
        $form['smartling']['job']['add_to_existing_job_tab']['container']['job_info']['target'],
        t('@name field is required.', [
          '@name' => t('Job Target Locales'),
        ])
      );
    }

    if (!empty($form_state['input']['add_to_existing_job_tab']['container']['job_info']['due_date']['date']) &&
      !empty($form_state['input']['add_to_existing_job_tab']['container']['job_info']['due_date']['time'])
    ) {
      $due_date = strtotime($form_state['input']['add_to_existing_job_tab']['container']['job_info']['due_date']['date'] . ' ' . $form_state['input']['add_to_existing_job_tab']['container']['job_info']['due_date']['time']);

      if ($due_date < time()) {
        form_error($form['smartling']['job']['add_to_existing_job_tab']['container']['job_info']['due_date'], t('Due date can not be in the past.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    ctools_include('ajax');
    ctools_add_js('ajax-responder');

    $errors = form_get_errors();

    if ($errors) {
      drupal_get_messages('error');

      $commands[] = ajax_command_replace(
        '#translation_result',
        '<div id="translation_result" class="failed">' . implode("<br>", $errors) . '</div>'
      );

      return [
        '#type' => 'ajax',
        '#commands' => $commands,
      ];
    }

    if (!smartling_is_configured()) {
      $commands[] = ajax_command_replace(
        '#translation_result',
        '<div id="translation_result" class="failed">' . t('Smartling connector is not configured') . '</div>'
      );

      return [
        '#type' => 'ajax',
        '#commands' => $commands,
      ];
    }

    $job_id = NULL;
    $job_authorize = FALSE;
    $drupal_target_locales = [];

    // Get job id: create job or select existing one.
    // Take into consideration ajax requests where triggering element can look
    // like "edit-create-new-job-tab-submit--N".
    if (strpos($form_state['triggering_element']['#id'], 'edit-create-new-job-tab-submit') !== FALSE) {
      $job_result = $this->submitCreateJob($form, $form_state);

      $job_id = $job_result['job_id'];
      $job_authorize = $job_result['authorize'];
      $drupal_target_locales = $job_result['target_locales'];
    }

    if (strpos($form_state['triggering_element']['#id'], 'edit-add-to-existing-job-tab-container-job-info-submit') !== FALSE) {
      $job_result = $this->submitAddToJob($form, $form_state);

      $job_id = $job_result['job_id'];
      $job_authorize = $job_result['authorize'];
      $drupal_target_locales = $job_result['target_locales'];
    }

    if (empty($job_id)) {
      $commands = [
        ctools_ajax_command_reload(),
      ];

      return [
        '#type' => 'ajax',
        '#commands' => $commands,
      ];
    }

    // Create Smartling Batch.
    $batch_result = $this->submitCreateBatch($job_id, $job_authorize);

    if (empty($batch_result['batch_uid'])) {
      $commands = [
        ctools_ajax_command_reload(),
      ];

      return [
        '#type' => 'ajax',
        '#commands' => $commands,
      ];
    }

    $items = array_filter(array_values($form_state['input']['items']));
    $items_count = count($items);

    foreach ($items as $k => $v) {
      if (empty($v)) {
        continue;
      }

      $v = explode('_||_', $v);
      $id = (int) $v[0];
      $entity_type = $v[1];
      $entity = entity_load_single($entity_type, $id);

      $res = [];
      try {
        $res = drupal_container()
          ->get('smartling.queue_managers.upload_router')
          ->routeUploadRequest(
            $entity_type,
            $entity,
            $drupal_target_locales,
            $job_id,
            $batch_result['batch_uid'],
            NULL,
            $items_count == ($k + 1)
          );
      } catch (SmartlingGenericException $e) {
        smartling_log_get_handler()->error($e->getMessage() . '   ' . $e->getTraceAsString());
        drupal_set_message($e->getMessage());
      }

      if ($res['status']) {
        drupal_set_message($res['message']);
      }
    }

    $commands = [
      ctools_ajax_command_reload(),
    ];

    return [
      '#type' => 'ajax',
      '#commands' => $commands,
    ];
  }
}
