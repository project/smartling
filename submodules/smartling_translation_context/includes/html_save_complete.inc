<?php
/**
 * @file
 * The htmlSaveComplete class can be used to save specified URLs completely in single file
 * by converting images to data URIs and extracting all CSS.
 *
 * Author: Sarfraz Ahmed
 * http://sarfraznawaz.wordpress.com
 */


//set_time_limit(300);

class HtmlSaveComplete {

  # url to save complete page from
  private $url = '';
  # holds parsed html
  private $cookie = '';
  private $html = '';
  # holds DOM object
  private $dom = '';


  protected static $authError = array(
    "response" => array(
      "code" => "AUTHENTICATION_ERROR",
      "data" => array("baseUrl" => NULL, "body" => NULL, "headers" => NULL),
      "messages" => array("Authentication token is empty or invalid."),
    ),
  );

  protected static $uriMissingError = array(
    "response" => array(
      "code" => "VALIDATION_ERROR",
      "data" => array("baseUrl" => NULL, "body" => NULL, "headers" => NULL),
      "messages" => array("fileUri parameter is missing."),
    ),
  );


  /**
   *
   */
  public function __construct() {
    # suppress DOM parsing errors
    libxml_use_internal_errors(TRUE);

    $this->dom = new DOMDocument();
    $this->dom->preserveWhiteSpace = FALSE;
    # avoid strict error checking
    $this->dom->strictErrorChecking = FALSE;
  }

  /**
   * Gets complete page data and returns generated string
   *
   * @param array $settings
   * @param string $url - url to retrieve
   * @param string $cookie - cookie for authorization
   * @param bool $keepjs - whether to keep javascript
   * @param bool $compress - whether to remove extra whitespaces
   * @return string|void
   * @throws \Exception
   */
  public function getCompletePage(array $settings, $url, $cookie = '', $keepjs = TRUE, $compress = FALSE) {
    # validate the URL
    if (!filter_var($url, FILTER_VALIDATE_URL)) {
      throw new Exception('Invalid URL. Make sure to specify http(s) part.');
    }

    if (empty($url)) {
      return self::$uriMissingError;
    }

    if (!$cookie) {
      return self::$authError;
    }

    $this->url = $url;
    $this->cookie = $cookie;
    $this->html = $this->getUrlContents($settings, $this->url);

    if (strlen($this->html) <= 300) {
      return '';
    }

    return ($compress) ? $this->compress($this->html) : $this->html;
  }

  /**
   * Checks whether or not remote file exists.
   *
   * @param $url
   * @param array $settings
   * @param int $connection_timeout
   * @param int $timeout
   *
   * @return bool
   */
  public function remote_file_exists($url, array $settings, $connection_timeout = 500, $timeout = 5000) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    # don't download content
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $connection_timeout);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);

    $this->applySettingsToCurl($settings, $ch);

    if (curl_exec($ch) !== FALSE) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Compresses generated page by removing extra whitespace
   */
  private function compress($string) {
    # remove whitespace
    return str_replace(array(
      "\r\n",
      "\r",
      "\n",
      "\t",
      '  ',
      '    ',
      '    '
    ), ' ', $string);
  }

  /**
   * Gets content for given url using curl and optionally using user agent
   *
   * @param array $settings
   * @param $url
   * @param int $timeout
   * @param string $user_agent
   * @return int|mixed
   */
  public function getUrlContents(
    array $settings,
    $url,
    $timeout = 0,
    $user_agent = 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.215 Safari/534.10'
  ) {
    $crl = curl_init();

    $this->applySettingsToCurl($settings, $crl);

    curl_setopt($crl, CURLOPT_URL, $url);
    curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1); # return result as string rather than direct output
    curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, $timeout); # set the timeout
    curl_setopt($crl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($crl, CURLOPT_COOKIE, $this->cookie);
    curl_setopt($crl, CURLOPT_USERAGENT, $user_agent); # set our 'user agent'

    curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);

    $output = curl_exec($crl);
    curl_close($crl);

    if (!$output) {
      return -1;
    }

    return $output;
  }

  /**
   * @param $settings
   * @param $curl
   */
  private function applySettingsToCurl($settings, $curl) {
    if (!empty($settings['basic_auth']['enabled'])) {
      curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($curl, CURLOPT_USERPWD, $settings['basic_auth']['login'] . ':' . $settings['basic_auth']['password']);
    }
  }

}
