<?php

/**
 * Uploads an HTML page to Smartling by a given URL and username
 * @param $settings
 * @param $url
 * @param bool|FALSE $force_show
 * @param null $file_name
 * @return mixed|string|void
 */
function smartling_translation_context_upload_context($settings, $url, $force_show = FALSE, $file_name = NULL) {
  $url .= (strpos($url, '?') === FALSE) ? '?' : '&';
  $url .= 'smartling_context=1';

  module_load_include('inc', 'smartling_translation_context', 'includes/context_authorize');
  module_load_include('inc', 'smartling_translation_context', 'includes/html_save_complete');

  try {
    $auth = smartling_translation_context_authorize($settings['username']);

    $page = new HtmlSaveComplete();
    $html = $page->getCompletePage($settings, $url, $auth['response']['data']['cookie']);
  } catch (\Exception $e) {
    smartling_log_get_handler()
      ->error('Error happened during context retrieval: @err', array('@err' => $e->getMessage()));
    return [];
  }

  if (!is_string($html)) {
    smartling_log_get_handler()
      ->error('Error happened during context retrieval: @err', array('@err' => print_r($html, TRUE)));
    return [];
  }

  if ($force_show) {
    return $html;
  }

  // Returns array with match id.
  $result = drupal_container()
    ->get('smartling.api_wrapper')
    ->uploadContext(array('url' => $url, 'html' => $html, 'file_name' => $file_name));


  if (empty($result)) {
    smartling_log_get_handler()->error('There was an error during context upload for entity with url: "@url" and user: "@user". Please <a href="@url_logs">see logs</a> for details.', array(
      '@url_logs' => url('admin/reports/dblog'),
      '@url' => $url,
      '@user' => $settings['username'],
    ));
  }
  else {
    smartling_log_get_handler()->info('Smartling uploaded context for an entity with url: "@url" and user: "@user".', array(
      '@url' => $url,
      '@user' => $settings['username'],
    ));
  }
  return $result;
}

/**
 * Wrapper around smartling_translation_context_upload_context().
 * Uploads context for a given entity.
 * @param $entity_type
 * @param $entity
 * @return mixed|string|void
 */
function smartling_translation_context_upload_entity($entity_type, $entity) {
  global $user;

  if ((empty($entity_type) || empty($entity))) {
    return;
  }

  if (isset($entity->smartling_context_url)) {
    $url = $entity->smartling_context_url;
  }
  else {
    $url = entity_uri($entity_type, $entity);
    $url = url($url['path'], array('absolute' => TRUE));
  }

  $name = isset($user->name) ? $user->name : user_load(1)->name;
  $username = variable_get('smartling_translation_context_username', $name);
  $basic_auth_enabled = variable_get('smartling_translation_context_basic_auth_enabled', FALSE);
  $basic_auth_login = variable_get('smartling_translation_context_basic_auth_login', '');
  $basic_auth_password = variable_get('smartling_translation_context_basic_auth_password', '');

  $entity_wrapper = entity_metadata_wrapper($entity_type, $entity);
  $entity_data = smartling_entity_load_by_conditions([
    'rid' => $entity_wrapper->getIdentifier(),
    'entity_type' => $entity_wrapper->type(),
  ]);
  $file_name = empty($entity_data) ? NULL : $entity_data->file_name;

  return smartling_translation_context_upload_context([
    'username' => $username,
    'basic_auth' => [
      'enabled' => $basic_auth_enabled,
      'login' => $basic_auth_login,
      'password' => $basic_auth_password,
    ],
  ], $url, FALSE, $file_name);
}

/**
 * Callback for cron handling.
 */
function smartling_translation_context_on_cron_run_upload() {
  $query = db_select('smartling_context', 'sc');
  $query->fields('sc', array('eid', 'entity_type', 'num_attempts'));
  $query->condition('sc.when_to_upload', REQUEST_TIME, '<');
  $query->condition('sc.status', SMARTLING_CONTEXT_STATUS_TO_UPLOAD, '=');
  $query->condition('sc.num_attempts', SMARTLING_CONTEXT_MAX_UPLOAD_ATTEMPTS, '<');
  $query->range(0, SMARTLING_CONTEXT_NUM_ITEMS_PER_CRON);
  $result = $query->execute()->fetchAll();

  foreach ($result as $v) {
    $entity = entity_load_single($v->entity_type, $v->eid);
    $res = smartling_translation_context_upload_entity($v->entity_type, $entity);

    if (is_null($res) || empty($res)) {
      db_update('smartling_context')
        ->fields(array(
          'num_attempts' => $v->num_attempts + 1,
        ))
        ->condition('eid', $v->eid)
        ->condition('entity_type', $v->entity_type)
        ->execute();
      continue;
    }

    db_merge('smartling_context')
      ->key(array('eid' => $v->eid, 'entity_type' => $v->entity_type))
      ->fields(array(
        'last_uploaded' => REQUEST_TIME,
        'status' => SMARTLING_CONTEXT_STATUS_UPLOADED,
      ))
      ->execute();
  }
}
