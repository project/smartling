<?php
/**
 * @file
 * smartling_media.features.inc
 */

/**
 * Implements hook_views_api().
 */
function smartling_media_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
