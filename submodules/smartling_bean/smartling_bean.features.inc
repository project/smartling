<?php
/**
 * @file
 * smartling_bean.features.inc
 */

/**
 * Implements hook_views_api().
 */
function smartling_bean_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
