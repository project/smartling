<?php
/**
 * @file
 * Install, update, and uninstall functions for the Smartling Quiz multichoice module.
 */

module_load_include('inc', 'field', 'field.crud');
module_load_include('module', 'smartling_quiz_multichoice');

/**
 * Implements hook_install().
 */
function smartling_quiz_multichoice_install() {
  field_info_cache_clear();

  // Make sure the field doesn't already exist.
  if (!field_info_field(SMARTLING_QUIZ_MULTICHOICE_FIELD_NAME)) {
    // Create the field.
    $field = [
      'field_name' => SMARTLING_QUIZ_MULTICHOICE_FIELD_NAME,
      'type' => SMARTLING_QUIZ_MULTICHOICE_FIELD_TYPE,
    ];
    field_create_field($field);

    // Create the instance.
    $instance = [
      'field_name' => SMARTLING_QUIZ_MULTICHOICE_FIELD_NAME,
      'entity_type' => 'node',
      'bundle' => 'multichoice',
      'label' => st('Alternatives (Smartling)'),
      'description' => st('Smartling defined field for handling Quiz multichoice answers'),
      'required' => TRUE,
    ];
    field_create_instance($instance);

    watchdog(
      'smartling_quiz_multichoice',
      t('Field !field_name was added successfully.', ['!field_name' => SMARTLING_QUIZ_MULTICHOICE_FIELD_NAME])
    );
  }
  else {
    watchdog(
      'smartling_quiz_multichoice',
      t('Field !field_name already exists.', ['!field_name' => SMARTLING_QUIZ_MULTICHOICE_FIELD_NAME])
    );
  }
}
