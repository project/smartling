<?php

/**
 * @file
 * Contains Drupal\smartling\FieldProcessors\QuizMultichoiceAnswersFieldProcessor.
 */

namespace Drupal\smartling_quiz_multichoice\FieldProcessors;

use Drupal\smartling\FieldProcessors\TextFieldProcessor;

class QuizMultichoiceAnswersFieldProcessor extends TextFieldProcessor {

  /**
   * {@inheritdoc}
   */
  public function getSmartlingContent() {
    $data = [];

    if (!empty($this->entity->alternatives)) {
      foreach ($this->entity->alternatives as $delta => $value) {
        if (empty($value['answer'])) {
          continue;
        }

        $data[$delta]['value'] = isset($value['answer']['value']) ? $value['answer']['value'] : '';
        $data[$delta]['format'] = isset($value['answer']['format']) ? $value['answer']['format'] : '';
      }
    }

    return $data;
  }

  public function setDrupalContentFromXML($field_value) {
    if (!empty($this->entity->alternatives)) {
      foreach ($this->entity->alternatives as $delta => &$value) {
        if (empty($field_value[$delta])) {
          continue;
        }

        $value['answer'] = $field_value[$delta];
      }
    }
  }
}
