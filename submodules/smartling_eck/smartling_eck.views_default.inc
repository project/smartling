<?php

/**
 * @file
 * smartling_eck.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function smartling_eck_views_default_views() {
  $export = array();

  // Place all ECK views after the others.
  $weight = 10;

  foreach (EntityType::loadAll() as $entity_type) {
    // Don't bother creating a page for any view that doesn't allow languages,
    // i.e. couldnt actually be translated.
    if (empty($entity_type->properties['language'])) {
      continue;
    }

    // Each ECK entity type has a primary parent table.
    $base_table = 'eck_' . $entity_type->name;
    // $schema = eck__entity_type__schema($entity_type);

    // Generate the view definition.
    $view = new view();
    $view->name = 'smartling_eck_' . $entity_type->name;
    $view->description = 'Admin page for bulk submitting objects for the ' . $entity_type->label . ' ECK entity type to the Smartling translation service.';
    $view->tag = 'smartling';
    $view->base_table = $base_table;
    $view->human_name = 'Smartling: ECK ' . $entity_type->name;
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    // Base translation strings, optional fields will be added later.
    $translatables[$view->name] = array(
      t('Master'),
      t('Entity'),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Items per page'),
      t('- All -'),
      t('Offset'),
      t('« first'),
      t('‹ previous'),
      t('next ›'),
      t('last »'),
      t('ECK'),
      t('- Choose an operation -'),
      t('Translate'),
      t('Entity ID'),
      t('Type'),
      t('.'),
      t(','),
      t('Edit link'),
      t('Page'),
    );

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'Entity';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'none';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['query']['options']['distinct'] = TRUE;
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '50';
    $handler->display->display_options['style_plugin'] = 'table';

    /* Relationship: Smartling: Smartling Entity */
    $handler->display->display_options['relationships']['smartling_entity_data']['id'] = 'smartling_entity_data';
    $handler->display->display_options['relationships']['smartling_entity_data']['table'] = 'eck_' . $entity_type->name;
    $handler->display->display_options['relationships']['smartling_entity_data']['field'] = 'smartling_entity_data';

    /* Field: Bulk operations: ECK */
    $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
    $handler->display->display_options['fields']['views_bulk_operations']['table'] = $base_table;
    $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
    $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '1';
    $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
    $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
    $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
    $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
    $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
      'action::smartling_do_translate_action' => array(
        'selected' => 1,
        'postpone_processing' => 0,
        'skip_confirmation' => 1,
        'override_label' => 1,
        'label' => 'Create job',
      ),
      'action::smartling_do_translate_add_to_job_action' => array(
        'selected' => 1,
        'postpone_processing' => 0,
        'skip_confirmation' => 1,
        'override_label' => 1,
        'label' => 'Add to job',
      ),
    );

    // The entity ID is mandatory.
    /* Field: ECK: Internal, numeric primary ID */
    $handler->display->display_options['fields']['id']['id'] = 'id';
    $handler->display->display_options['fields']['id']['table'] = $base_table;
    $handler->display->display_options['fields']['id']['field'] = 'id';
    $handler->display->display_options['fields']['id']['label'] = 'Entity ID';

    // The entity bundle is mandatory.
    /* Field: ECK: Type */
    $handler->display->display_options['fields']['type']['id'] = 'type';
    $handler->display->display_options['fields']['type']['table'] = $base_table;
    $handler->display->display_options['fields']['type']['field'] = 'type';
    $handler->display->display_options['fields']['type']['label'] = 'Type';

    // Optional fields depending upon the ECK entity type's settings.
    /* Field: ECK: Title */
    if (isset($entity_type->properties['title'])) {
      $handler->display->display_options['fields']['title']['id'] = 'title';
      $handler->display->display_options['fields']['title']['table'] = $base_table;
      $handler->display->display_options['fields']['title']['field'] = 'title';

      $translatables[$view->name][] = t('Title');
    }

    // Optional fields depending upon the ECK entity type's settings.
    /* Field: ECK: UID / Author */
    if (isset($entity_type->properties['uid'])) {
      $handler->display->display_options['fields']['uid']['id'] = 'uid';
      $handler->display->display_options['fields']['uid']['table'] = $base_table;
      $handler->display->display_options['fields']['uid']['field'] = 'uid';

      $translatables[$view->name][] = t('Author');
    }

    /* Field: ECK: Language */
    $handler->display->display_options['fields']['language']['id'] = 'language';
    $handler->display->display_options['fields']['language']['table'] = $base_table;
    $handler->display->display_options['fields']['language']['field'] = 'language';
    $translatables[$view->name][] = t('Language');

    /* Field: ECK: Date created */
    if (isset($entity_type->properties['created'])) {
      $handler->display->display_options['fields']['created']['id'] = 'created';
      $handler->display->display_options['fields']['created']['table'] = $base_table;
      $handler->display->display_options['fields']['created']['field'] = 'created';
      $handler->display->display_options['fields']['created']['date_format'] = 'short';
      $handler->display->display_options['fields']['created']['second_date_format'] = 'long';

      $translatables[$view->name][] = t('Date created');
    }

    /* Field: ECK: Date changed */
    if (isset($entity_type->properties['changed'])) {
      $handler->display->display_options['fields']['changed']['id'] = 'changed';
      $handler->display->display_options['fields']['changed']['table'] = $base_table;
      $handler->display->display_options['fields']['changed']['field'] = 'changed';
      $handler->display->display_options['fields']['changed']['date_format'] = 'short';
      $handler->display->display_options['fields']['changed']['second_date_format'] = 'long';

      $translatables[$view->name][] = t('Date changed');
    }

    /* Field: Smartling: Locales for ECK */
    $handler->display->display_options['fields']['locale_info']['id'] = 'locale_info';
    $handler->display->display_options['fields']['locale_info']['table'] = $base_table;
    $handler->display->display_options['fields']['locale_info']['field'] = 'locale_info';

    // This field is added at the end.
    /* Field: ECK: Edit link */
    $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
    $handler->display->display_options['fields']['edit_link']['table'] = $base_table;
    $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';

    /* Filter criterion: Home page elements: type */
    $handler->display->display_options['filters']['type']['id'] = 'type';
    $handler->display->display_options['filters']['type']['table'] = $base_table;
    $handler->display->display_options['filters']['type']['field'] = 'type';
    $handler->display->display_options['filters']['type']['group'] = 1;
    $handler->display->display_options['filters']['type']['exposed'] = TRUE;
    $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
    $handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
    $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
    $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';

    /* Filter criterion: Smartling Entity Data: Smartling status */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'smartling_entity_data';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['relationship'] = 'smartling_entity_data';
    $handler->display->display_options['filters']['status']['group'] = 1;
    $handler->display->display_options['filters']['status']['exposed'] = TRUE;
    $handler->display->display_options['filters']['status']['expose']['operator_id'] = 'status_op';
    $handler->display->display_options['filters']['status']['expose']['label'] = 'Smartling status';
    $handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
    $handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';

    /* Filter criterion: Entity language */
    $handler->display->display_options['filters']['language']['id'] = 'language';
    $handler->display->display_options['filters']['language']['table'] = $base_table;
    $handler->display->display_options['filters']['language']['field'] = 'language';
    $handler->display->display_options['filters']['language']['operator'] = 'not in';
    $handler->display->display_options['filters']['language']['value'] = array(
      'und' => 'und',
    );

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['path'] = 'admin/content/smartling-content/' . $entity_type->name;
    $handler->display->display_options['menu']['type'] = 'tab';
    $handler->display->display_options['menu']['title'] = 'ECK ' . $entity_type->label;
    $handler->display->display_options['menu']['weight'] = $weight++;
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['menu']['context_only_inline'] = 0;

    $export[$view->name] = $view;
  }

  return $export;
}
