(function ($) {

    Drupal.behaviors.smartlingJobsUtcToLocalTime = {
        attach: function (context, settings) {
            var utcTimeString = $('input[name="add_to_existing_job_tab[container][job_info][utc_due_date_hidden]"]', context).val();

            if (utcTimeString) {
                var utcTime = moment.utc(utcTimeString).toDate();
                var localTime = moment(utcTime);

                $('input[name="add_to_existing_job_tab[container][job_info][due_date][date]"]', context).val(localTime.format('MM/DD/YYYY'));
                $('input[name="add_to_existing_job_tab[container][job_info][due_date][time]"]', context).val(localTime.format('HH:mm'));
            }

            $('input[name="smartling_users_time_zone"]', context).val(moment.tz.guess());
        }
    };

})(jQuery);
