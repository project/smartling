(function ($) {

    Drupal.behaviors.smartlingJobName = {
        attach: function (context, settings) {
            var $job_elements = $('td.column-job-name');
            var processed_batches = [];

            for (var i = 0; i < $job_elements.length; i++) {
                var job_uid = $($job_elements[i]).attr('job-uid');

                if ($.inArray(job_uid, processed_batches) == -1) {
                    processed_batches.push(job_uid);

                    $.ajax({
                        url: '/admin/config/regional/smartling/batch-list/job/' + job_uid,
                        type: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            $('[job-uid="' + response.translationJobUid + '"]').hide().empty().append(response.jobName).fadeIn(500);
                        }
                    });
                }
            }
        }
    };

    Drupal.behaviors.smartlingBatchStatus = {
        attach: function (context, settings) {
            setInterval(function() {
                $.ajax({
                    url: '/admin/config/regional/smartling/batches',
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        if (response.hasOwnProperty('items')) {
                            for (var i = 0; i < response.items.length; i++) {
                                var batch_info = response.items[i],
                                    batch_uid = batch_info.batchUid,
                                    $tr = $('[batch-uid="' + batch_uid + '"]'),
                                    $column_status = $tr.find('td.column-status'),
                                    $column_error = $tr.find('td.column-error'),
                                    tr_status = $column_status.text();

                                if (batch_info.status != tr_status) {
                                    $column_status.hide().text(batch_info.status).fadeIn(500);

                                    if (batch_info.status == 'Completed') {
                                        $tr.removeClass('batch-not-completed').removeClass('ok').removeClass('error').addClass('batch-completed');
                                        $column_error.hide().empty().append('<div></div>').fadeIn(500);

                                        if (batch_info.hasError) {
                                            $tr.addClass('error');
                                        }
                                        else {
                                            $tr.addClass('ok');
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }, 5000);
        }
    }

})(jQuery);
